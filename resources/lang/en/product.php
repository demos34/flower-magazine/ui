<?php

return [
    'size' => 'size:',
    'color' => 'color:',
    'more-description' => 'more',
    'button' => 'add to your cart',
    'less-description' => 'less',
    'alert' => 'This product is out of Stock. Please, contact with us to provide it to You or - just choose another one!',
    'success' => 'In Stock',
    'out' => 'Out of Stock',
];
