<?php

return [
    'pol' =>'Policy',
    'policy' =>'Summary',
    'security' =>'Security policy',
    'summary' =>'Terms and conditions',
    'policy-cookies' => 'Cookies Policy',
    'cookies' => 'Cookies',
    'button-hide' => 'Hide',
    'button-show' => 'More ...',
];
