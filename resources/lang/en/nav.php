<?php

return [
    'about' => 'about us',
    'products' => 'products',
    'contacts' => 'contacts',
    'cart-your' => 'yours',
    'cart-order' => 'purchases',
    'cart-price' => 'lv.',
];
