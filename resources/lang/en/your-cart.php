<?php
return [
    'title' => 'Your cart',
    'title-summary' => 'Summary:',
    'your-cart' => 'your cart',
    'delete' => 'X',
    'sum' => 'Total',
    'purchase' => 'Purchase',
    'end' => 'finish',
];
