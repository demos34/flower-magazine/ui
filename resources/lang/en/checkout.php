<?php

return [
    'names' =>'Names',
    'phone' =>'Telephone',
    'address' =>'Address',
    'email' =>'Email',
    'additional' =>'Additional info',
    'textarea-placeholder' =>'Additional information - i.e. when you want to get products',
];
