<?php

return [
    'h2' => 'COOKIES POLICY',
    'p-one' => 'We, Ley’s team, want to inform You about Privacy and how We use your data.',
    'p-two' => 'We use Cookies to improve your experience in our product. Those Cookies never save your data in our servers and we cannot recognize You as a person.',
    'p-three' => 'Cookie is a small text document, which is saved in your browser.
             Also, Cookies make your experience faster and secure and keep your preferences (i.e. the language).
            Cookies send information back to us (first-party cookie) or to another website (third-party cookie).',
    'p-four' => 'You can disable the Cookies, but this action can be unfavorable for some parts of our site.',
    'p-five' => 'In some cases, we need your permission to use Cookies. You can allow or disallow it (permission) in Settings of your browser',
    'h3-one' => 'Used Cookies',
    'p-six' =>'Used Cookies can be categorized by:',
    'li-one' => 'Necessary Cookies – allows You to surf in the website and use basic functionality.
                Those Cookies are installed in response of your experience in the website and they are like service.
                Those Cookies are extremely necessary!',
    'li-two' =>'Functionality Cookies – We use them to recognize you next time, when you want to visit our website.
                Those Cookies collect anonymous information and can’t track your moves.
                They are valid for 2 years.',
    'li-three' =>'Analytics Cookies – they are collecting information for count of visitors and how users use our services.
                Those Cookies improves our work and help us to make our website easy for you. They are valid for 2 years.',
    'li-four' => 'Ads Cookies save information about your visit and used pages and links.
                we will use that information for show you ads, which are interested for You.
                Those cookies are used and for limit count of ads, which are showed to You.
                We can share information, collected by those Cookies with third-party (like as Advertisers).
                You can change settings about those cookies in your browser in every time!.
                Those cookies are valid for 2 years.',
    'li-five' => 'Social media Cookies allows You to share your experience in social media like Facebook and Twitter. Those cookies are out of our control',
    'li-six' => 'If we use other Cookies, we will inform You!',
    'p-seven' => 'Note: We want to inform You – We use third-party services to analyses your experience ',
    'h3-two' => 'Cookies management',
    'p-eight' => 'If You want – You can disallow your permission for cookies by changing settings of your browser, deleting cache and cookie data and set to block our cookies. For further information – visit your browser’s creators site',
    'p-nine' => 'OS of your mobile device can give You opportunity to set different options for Cookies.',


];
