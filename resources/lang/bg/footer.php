<?php

return [
    'pol' =>'Политики',
    'policy' =>'Общи',
    'security' =>'Политики за сигурност',
    'summary' =>'Общи политики на сайта',
    'policy-cookies' => 'Бисквитки',
    'cookies' => 'Бисквитки',
    'button-hide' => 'Скрий',
    'button-show' => 'Още ...',
];
