<?php

return [
    'about' => 'за нас',
    'products' => 'продукти',
    'contacts' => 'контакти',
    'cart-your' => 'твоите',
    'cart-order' => 'покупки',
    'cart-price' => 'лв.',
];
