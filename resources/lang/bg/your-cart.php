<?php
return [
    'title' => 'Вашата количка',
    'title-summary' => 'Последни стъпки:',
    'your-cart' => 'вашата количка',
    'delete' => 'X',
    'sum' => 'обща сума',
    'purchase' => 'поръчай',
    'end' => 'край',
];
