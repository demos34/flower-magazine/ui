<div>
    <div style="font-size: 2em; border: solid 1px #1d2124;">
        <span class="mx-3" style="border: none; background-color: lightyellow; height: 10px; width: 1em;" wire:click="decrement">-</span>
        <input
            name="quantity"
            min="0"
            class="text-center"
            style="border:none; height: 1em; width: 3em; background-color: lightyellow" value="{{$quantity}}">
        <span class="mx-3" style="border: none; background-color: lightyellow; height: 1em; width: 1em;" wire:click="increment">+</span>
    </div>
</div>
