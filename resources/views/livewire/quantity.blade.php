<div>
    <div id="live-wire-quantity-div" style="">
        <span class="mx-3 class-minus" style="" wire:click="decrement">-</span>
        <input
            name="quantity"
            min="0"
            class="text-center class-input"
            style="" value="{{$quantity}}">
        <span class="mx-3 class-plus" style="" wire:click="increment">+</span>
    </div>
</div>
