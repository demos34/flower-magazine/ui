@extends('layouts.app')

@section('title')
    Leya | {{$category->name}}
@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="animated-cat">
        {{--        <video autoplay="autoplay" muted loop id="animated-cat" playsinline>--}}
        {{--            <source src="/storage/style/animated-roses.mp4" type="video/mp4">--}}
        {{--        </video>--}}
        <video autoplay="autoplay" muted loop id="animated-cat" playsinline>
            <source src="{{$category->anim}}" type="video/mp4">
        </video>
    </div>
    <div class="container" id="category-wrapper">
        <div id="products" class="flowers">
            <div class="product-wrapper-header">
                <div class="product-header-hr" id="hr">
                </div>
                <div class="product-header-div font-weight-bold" id="text">
                    {{$category->name}}
                </div>
                <div class="select-div text-center">
                    <select class="classic" onchange="location = this.value;">
                        <option>{{__('category-all.label-sort')}}</option>
                        @foreach($sort as $sortBy)
                            <option value="/{{App::getLocale()}}/category/sort/{{$sortBy->id}}/{{$category->id}}">
                                {{$sortBy->sort}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="flowers-colors" id="mobile">
                    <label>
                        {{__('category-all.label-color')}}
                    </label>
                    <div class="row my-1" id="color-wrapper">
                        @foreach($colors as $color)
                            @if($color->id > 1)
                                <div class="col-sm-3">
                                    <a class="color-link"
                                       href="/{{App::getLocale()}}/category/color/{{$color->id}}/{{$category->id}}">
                                        <img class="color-image rounded-circle" src="{{$color->image}}" alt="">
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="my-5" id="content-wrapper">
                    <div class="flowers-colors" id="desk">
                        <label>
                            <a class="color-link" href="/{{App::getLocale()}}/category/color/1/{{$category->id}}">
                                {{__('category-all.label-color')}}
                            </a>
                        </label>
                        <div class="row my-1" id="color-wrapper">
                            @foreach($colors as $color)
                                @if($color->id > 1)
                                    <div class="col-sm-3">
                                        <a href="/{{App::getLocale()}}/category/color/{{$color->id}}/{{$category->id}}">
                                            <img class="color-image rounded-circle" src="{{$color->image}}" alt="">
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="flowers-products">
                        <div class="row " id="flowers-product-wrapper">
                            @foreach($products as $product)
                                <div class="col-lg-4 flower-item-col" id="flower-item-wrapper">
                                    <a href="/{{App::getLocale()}}/products/{{$product->slug}}">
                                        <img class="flower-product-image" src="{{$product->image}}" alt="">
                                        <br>
                                        <span class="justify-content-center ml-2">
                                        {{$product->price}} {{__('nav.cart-price')}}
                                        </span>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('custom-scripts')
    <script src="{{ asset('js/lines.js') }}"></script>
@endsection
