@extends('layouts.app')

@section('title')
    Leya | {{__('category-all.title-types')}}
@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="animated-cat">
        {{--        <video autoplay="autoplay" muted loop id="animated-cat" playsinline>--}}
        {{--            <source src="/storage/style/animated-roses.mp4" type="video/mp4">--}}
        {{--        </video>--}}
        <video autoplay="autoplay" muted loop id="animated-cat" playsinline>
            <source src="{{$type->anim}}" type="video/mp4">
        </video>
    </div>
    <div class="container" id="wrapper">
        <div id="products" class="flowers">
            <div class="product-wrapper-header">
                <div class="product-header-hr" id="hr">
                </div>
                <div class="product-header-div font-weight-bold" id="text">
                    {{$type->name}}
                </div>
            </div>
            <div id="flowers" class="row">
                @foreach($categories as $category)
                    @if($front === FALSE)
                        <div class="col-lg-6  my-auto text-center mobile">
                            <a href="/{{App::getLocale()}}/category/show/{{$category->slug}}">
                                <div class="flower-wrapper my-4 align-items-center">
                                    <img class="product-image" src="{{$category->image}}" alt="">
                                    <div id="flower-name">
                                        <span>
                                            {{$category->name}}
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @else
                        <div class="col-lg-6  my-auto text-center mobile">
                            <a href="/{{App::getLocale()}}/category/show/{{$category->slug}}">
                                <div class="flower-wrapper my-4 align-items-center">
                                    <img class="product-image" src="{{$category->image}}" alt="">
                                    <div id="flower-wrappers-name">
                                        <span>
                                            {{$category->name}}
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts')
    <script src="{{ asset('js/lines.js') }}"></script>
@endsection
