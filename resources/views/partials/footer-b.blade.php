<footer class="footer-class">
    <div class="footer-wrapper d-flex justify-content-end">
        <div class="footer-item">
            <img class="footer-image" src="/storage/style/banner-black.png" alt="">
        </div>
        <div id="security-policy" class="footer-item">
            <span class="text-center">{{__('footer.pol')}}</span><br>
            <span class="text-center">{{__('footer.policy')}}</span><br>
            <a href="/policy/security">{{__('footer.security')}}</a>
            <br>
            <a href="/policy/summary">{{__('footer.summary')}}</a>
            <br>
            <span class="text-center mt-2">{{__('footer.policy-cookies')}}</span>
            <br>
            <a href="/policy/cookies">{{__('footer.cookies')}}</a>
        </div>
{{--        <div id="cookies-policy" class="footer-item">--}}

{{--        </div>--}}
        <div id="contacts" class="footer-item">
            <div>
                <div class="text-center" id="mobile-policy-hidden">
                    <a href="/policy/cookies">{{__('footer.cookies')}}</a>
                    <br>
                    <a href="/policy/security">{{__('footer.security')}}</a>
                    <br>
                    <a href="/policy/summary">{{__('footer.summary')}}</a>
                    <br>
                    <button id="hide-more" class="mobile-policy-button hide" onclick="hideMore()">{{__('footer.button-hide')}}</button>
                </div>
                <div class="contact-mobile" id="contact-mobile">
                    <img class="footer-contact-images" src="/storage/style/phone.png" alt=""/>
                    <span>+359 888 888 888</span>
                    <br>
                    <img class="footer-contact-images" src="/storage/style/email.png" alt=""/>
                    <span>leya@leya.bg</span>
                    <br>
                    <a href="https://www.instagram.com/flower_shop_leya/?hl=en&fbclid=IwAR3B3ojpoXetOoHlByHqJmkiKRJVlsbjoPF-nQgBNeKSASuYI7AZx4jhKJA" target="_blank">
                        <img class="footer-contact-images" src="/storage/style/insta.png" alt=""/>
                        <span>Leya's Instagram</span>
                    </a>
                    <br>
                    <a href="https://www.facebook.com/Flower-Shop-Leya-372173890220143/?ref=page_internal" target="_blank">
                        <img class="footer-contact-images" src="/storage/style/facebook.png" alt=""/>
                        <span>Leya's facebook</span>
                    </a>
                    <br>
                </div>
                <button id="show-more" class="mobile-policy-button show mx-auto" onclick="showMore()">{{__('footer.button-show')}}</button>
            </div>
        </div>
    </div>
</footer>
