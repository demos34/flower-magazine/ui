<div class="navbar-div-wrapper" style="">
</div>
<div class="navbar-div">
    <nav class="navbar navbar-expand-md navbar-nav-tag my-auto">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto" id="nav">
                <li class="nav-item">
                    <a class="nav-link nav-a" href="/">
                        {{__('nav.about')}}
                    </a>
                </li>
                <li id="dropdown-nav" class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="/category" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{__('nav.products')}} <span class="caret"></span>
                    </a>
                    <div id="nav-dropdown-div" class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                        @foreach($types as $type)
                            <a class="dropdown-item" href="/{{App::getLocale()}}/category/index/{{$type->slug}}">
                                {{$type->name}}
                            </a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-a" href="/about/">
                        {{__('nav.contacts')}}
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="d-flex justify-content-center">
{{--        <img class="nav-image" src="/storage/style/nav-image.png" alt="">--}}
    </div>
    <div id="right-section">
{{--        <div id="dropdown-nav" class="nav-item dropdown">--}}
{{--                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="/category" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
{{--                        {{App::getLocale()}} <span class="caret"></span>--}}
{{--                    </a>--}}
{{--                    <div id="nav-dropdown-div" class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">--}}
{{--                        <a class="dropdown-item" href="/lang/en">--}}
{{--                            EN--}}
{{--                        </a>--}}
{{--                        <a class="dropdown-item" href="/lang/bg">--}}
{{--                            BG--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--        </div>--}}
        <div class="cart-wrapper">
            <a href="/cart">
                <img src="/storage/style/cart.png" alt="" class="image-cart">
            </a>
            <div class="image-cart-span">
                <span>{{$count}}</span>
            </div>
        </div>
        <div class="row span-wrapper">
            <div class="col-sm-10 cart-div">
                <div class="cart-orders">{{__('nav.cart-your')}}</div>
            </div>
            <div class="col-sm-10 cart-div">
                <div class="cart-orders">{{__('nav.cart-order')}}</div>
            </div>
            <div class="col-sm-10 cart-div">
                <div class="cart-value">{{$total}} {{__('nav.cart-price')}}</div>
            </div>
        </div>
        <div class="search-wrapper">
            <a href="" class="search-wrapper">
                <img src="/storage/style/search.png" alt="" class="image-search">
            </a>
        </div>
    </div>
</div>

