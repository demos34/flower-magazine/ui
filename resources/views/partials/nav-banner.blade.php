<div class="nav-banner">
    <div class="banner-navbar fixed-top">
        <nav class="navbar navbar-expand-md shadow-sm" id="navbar-three">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul id="banner-ul" class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link nav-a" href="/">
                                {{__('nav.about')}}
                            </a>
                        </li>
                        <li id="dropdown-nav" class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{__('nav.products')}} <span class="caret"></span>
                            </a>
                            <div id="nav-dropdown-div" class="dropdown-menu dropdown-menu-left"
                                 aria-labelledby="navbarDropdown">
                                @foreach($types as $type)
                                    <a class="dropdown-item" href="/{{App::getLocale()}}/category/index/{{$type->slug}}">
                                        {{$type->name}}
                                    </a>
                                @endforeach
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-a" href="/about/">
                                {{__('nav.contacts')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="banner-right-section-nav">
                    <div class="banner-lang-swap">
                        <div id="dropdown-nav" class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="/category" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{App::getLocale()}} <span class="caret"></span>
                            </a>
                            <div id="nav-dropdown-div" class="dropdown-menu dropdown-menu-left"
                                 aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/lang/en">
                                    EN
                                </a>
                                <a class="dropdown-item" href="/lang/bg">
                                    BG
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="banner-cart-wrapper">
                        <a href="/cart">
                            <img src="/storage/style/cart.png" alt="" class="banner-image-cart">
                        </a>
                        <div class="banner-image-cart-span">
                            <span>{{$count}}</span>
                        </div>
                    </div>
                    <div class="row banner-span-wrapper">
                        <div class="col-sm-10 banner-cart-div">
                            <div class="banner-cart-orders">{{__('nav.cart-your')}}</div>
                        </div>
                        <div class="col-sm-10 banner-cart-div">
                            <div class="banner-cart-orders">{{__('nav.cart-order')}}</div>
                        </div>
                        <div class="col-sm-10 banner-cart-div">
                            <div class="banner-cart-value">{{$total}} {{__('nav.cart-price')}}</div>
                        </div>
                    </div>
                    <div class="banner-search-wrapper">
                        <a href="" class="banner-search-wrapper">
                            <img src="/storage/style/search.png" alt="" class="banner-image-search">
                        </a>
                    </div>
                </div>
            </div>
        </nav>
        <div id="banner-right-section">
            <div class="banner-lang-swap">
                <div id="dropdown-nav" class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="/category" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{App::getLocale()}} <span class="caret"></span>
                    </a>
                    <div id="nav-dropdown-div" class="dropdown-menu dropdown-menu-left"
                         aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/lang/en">
                            EN
                        </a>
                        <a class="dropdown-item" href="/lang/bg">
                            BG
                        </a>
                    </div>
                </div>
            </div>
            <div class="banner-cart-wrapper">
                <a href="/cart">
                    <img src="/storage/style/cart.png" alt="" class="banner-image-cart">
                </a>
                <div class="banner-image-cart-span">
                    <span>{{$count}}</span>
                </div>
            </div>
            <div class="row banner-span-wrapper">
                <div class="col-sm-10 banner-cart-div">
                    <div class="banner-cart-orders">{{__('nav.cart-your')}}</div>
                </div>
                <div class="col-sm-10 banner-cart-div">
                    <div class="banner-cart-orders">{{__('nav.cart-order')}}</div>
                </div>
                <div class="col-sm-10 banner-cart-div">
                    <div class="banner-cart-value">{{$total}} {{__('nav.cart-price')}}</div>
                </div>
            </div>
            <div class="banner-search-wrapper">
                <a href="" class="banner-search-wrapper">
                    <img src="/storage/style/search.png" alt="" class="banner-image-search">
                </a>
            </div>
        </div>
    </div>
</div>
