<div class="container my-3 text-center alerts">
    @if(session('success'))
        <div class="alert alert-success alert-style" role="alert">
            {{ session('success') }}
        </div>
    @endif

    @if(session('alert'))
        <div class="alert-danger text-center">
            {{session('alert')}}
        </div>
    @endif

    @if(session('danger'))
        <div class="alert alert-danger" role="alert">
            {{ session('danger') }}
        </div>
    @endif

    @if(session('warning'))
        <div class="alert alert-warning" role="alert">
            {{ session('warning') }}
        </div>
    @endif
</div>
