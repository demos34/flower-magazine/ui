<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slider</title>
    @include('../partials.fonts')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/banner.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/products.css') }}" rel="stylesheet">
    <link href="{{ asset('css/flowers.css') }}" rel="stylesheet">
    <link href="{{ asset('css/category.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cart.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slider.css') }}" rel="stylesheet">
</head>
<body>
<div class="d-flex justify-content-center">
    <div id="products" class="w-75 text-center">
        <div class="product-wrapper-header">
            <div class="product-header-hr">
            </div>
            <div class="product-header-div font-weight-bold">
                Продукти
            </div>
        </div>
        <div class="d-flex justify-content-between slider-wrapper">
            <button class="btn-slider btn-prev" onclick="prev()"><</button>
                <div class="d-flex justify-content-center slider-product-wrapper">
                    <div class="text-center text-decoration-none sliders is-visible">
                        <a href="/{{App::getLocale()}}/products/slug" class="product-a">
                            <img class="slider-product-image" src="/storage/flowers/flowers-rose.jpg" alt="">
                            <br>
                            <span class="justify-content-center mx-auto">
                                10lv
                            </span>
                        </a>
                    </div>
                <div class="text-center text-decoration-none sliders is-visible">
                    <a href="/{{App::getLocale()}}/products/slug" class="product-a">
                        <img class="slider-product-image" src="/storage/flowers/flowers-orchid.png" alt="">
                        <br>
                        <span class="justify-content-center mx-auto">
                            10 lv
                        </span>
                    </a>
                </div>
                <div class="text-center text-decoration-none sliders is-visible">
                    <a href="/{{App::getLocale()}}/products/slug" class="product-a">
                        <img class="slider-product-image" src="/storage/flowers/flowers-others-one.jpg" alt="">
                        <br>
                        <span class="justify-content-center mx-auto">
                                10lv
                            </span>
                    </a>
                </div>
                <div class="text-center text-decoration-none sliders is-visible">
                    <a href="/{{App::getLocale()}}/products/slug" class="product-a">
                        <img class="slider-product-image" src="/storage/flowers/flowers-others-two.jpg" alt="">
                        <br>
                        <span class="justify-content-center mx-auto">
                                10lv
                            </span>
                    </a>
                </div>
                <div class="text-center text-decoration-none sliders is-visible">
                    <a href="/{{App::getLocale()}}/products/slug" class="product-a">
                        <img class="slider-product-image" src="/storage/flowers/flowers-orchid.png" alt="">
                        <br>
                        <span class="justify-content-center mx-auto">
                                10lv
                            </span>
                    </a>
                </div>
            </div>
            <button class="btn-slider btn-next" id="btn-next" onclick="next()">></button>
        </div>
        <hr class="product-hr">
    </div>
</div>
<script src="{{ asset('js/slider.js') }}"></script>
</body>
</html>
