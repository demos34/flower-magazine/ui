@extends('layouts.app')

@section('title')

@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="container" id="wrapper">
        <div id="content-wrapper" class="font-weight-bold">
            <section class="home-para">
                <p class="text-center">
                    Красотата на природата е нещото, което винаги ни е вдъхновявало.
                    Затова и създаването на онлайн магазин, в който да ви предложим
                    най-прекрасните цветя, на най-добри цени, в съчетание с добро обслужване - е
                    мисия номер 1 за нас
                </p>
                <aside id="home-aside-bar" class="ads">
                    <p class="text-center">
                        Красотата на природата е нещото, което винаги ни е вдъхновявало.
                        Затова и създаването на онлайн магазин, в който да ви предложим
                        най-прекрасните цветя, на най-добри цени, в съчетание с добро обслужване - е
                        мисия номер 1 за нас
                    </p>
                </aside>
                <p class="text-center">
                    В магазин „Лея“ се стремим да ви дадем възможност да предоставите
                    подарък на себе си или на любим човек, който да остави траен и красив спомен
                    в съзнанието – а какво по-хубаво от живо цвете?
                </p>

                <p class="text-center">
                    Работата с клиентите за нас е като грижата за живите цветя – имат
                    нужда от много любов и внимание, затова се стремим да ви предоставим:
                </p>
                <ul>
                    <li> Цветя от най-висок клас</li>
                    <li> Красиви опаковки, с които да ги подарите</li>
                    <li> Блестящо обслужване</li>
                    <li> Сигурна и навременна доставка</li>
                </ul>
                <p class="text-center">
                    Ние не доставяме просто подаръци, а щастие, опаковано с много любов и
                    красота за Вас и хората, които обичате!
                </p>
                <p class="text-center">
                    Приятно пазаруване!
                </p>
            </section>
            <section>
                <p class="we">
                    Екипът на магазин „Леа“
                </p>
            </section>
        </div>
        <hr>
        <div id="products">
            <div class="product-wrapper-header">
                <div class="product-header-hr">
                </div>
                <div class="product-header-div font-weight-bold">
                    Продукти
                </div>
            </div>
            <div class="product-wrapper">
                <a href="{{ env('BACKEND_URL') }}">Backend</a>
            </div>
            <hr class="product-hr">
            <div class="product-wrapper">

            </div>
        </div>
    </div>
@endsection
