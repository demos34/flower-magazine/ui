@extends('layouts.app')

@section('title')
    Leya | {{__('home.title')}}
@endsection

@section('content')
    <div class="container">
        <div id="contact-wrapper">
            <div class="w-100">
                <section class="w-100">
                    <div class="send-us-msg-image text-center">
                        <div class="send-us-msg-wrapper text-center">
                            <p class="msg-title">
                                {{__('home.send-us-msg-title')}}
                            </p>
                            <p class="msg-body" id="desc-contact">
                                {{__('home.send-us-msg-body')}}
                            </p>
                            <p class="msg-body" id="mobile-one-contact">
                                {{__('home.send-us-msg-body-1')}}
                            </p>
                            <p class="msg-body" id="mobile-two-contact">
                                {{__('home.send-us-msg-body-2')}}
                            </p>
                            <p class="msg-body" id="mobile-three-contact">
                                {{__('home.send-us-msg-body-3')}}
                            </p>
                            <div class="send-us-form-wrapper">
                                <form class="send-us-msg-form" action="{{route('message')}}" method="post">
                                    @csrf
                                    <div class="my-auto">
                                        <input
                                            class="@error('email') is-invalid @enderror"
                                            id="email"
                                            name="email"
                                            type="email"
                                            placeholder="email..."
                                            value="{{ old('email') }}"
                                            required autocomplete="phone">
                                        <button class="send-us-msg-btn">{{__('home.send-us-msg-btn')}}</button>
                                        @error('email')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <br>
                                    <textarea class="@error('msg') is-invalid @enderror"
                                              id="msg"
                                              name="msg"
                                              type="msg"
                                              required autocomplete="msg">{{ old('email') }}</textarea>
                                    @error('msg')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </form>
                            </div>
                        </div>
                        <div class="contact-us-wrapper">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <a href="">
                                        <img class="contact-images" src="/storage/style/phone.png" alt="phone">
                                    </a><span class="contact-text">+359888 888 888</span>
                                </div>
                                <div>
                                    <a href="">
                                        <img class="contact-images" src="/storage/style/insta.png" alt="insta">
                                    </a> <span class="contact-text" id="span-insta">+359888 888 888</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <a href="">
                                        <img class="contact-images" src="/storage/style/email.png" alt="email">
                                    </a><span class="contact-text">+359888 888 888</span>
                                </div>
                                <div>
                                    <a href="">
                                        <img class="contact-images" src="/storage/style/facebook.png" alt="fb">
                                    </a><span class="contact-text" id="span-fb">+359888 888 888</span>
                                </div>
                            </div>
                        </div>
                        <div class="my-auto contact-us-strong">
                            <strong>
                                {{__('home.contact-us')}}
                            </strong>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
