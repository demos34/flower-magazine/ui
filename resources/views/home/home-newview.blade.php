@extends('layouts.app')

@section('title')
    Leya | {{__('home.title')}}
@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="animated">
        <video autoplay="autoplay" muted loop id="animated">
            <source src="storage/style/animated.mp4" type="video/mp4">
        </video>
    </div>
    <div class="container" id="wrapper">
        <div class="d-flex justify-content-center">
            <img class="home-image" src="/storage/style/nav-image.png" alt="">
        </div>
        <div id="content-wrapper" class="font-weight-bold">
            <section class="home-para">
                <p class="text-center">
                    {{ __('home.p1') }}
                </p>
                <p class="text-center">
                    {{ __('home.p2') }}
                </p>

                <p class="text-center">
                    {{ __('home.p3') }}
                </p>
                <ul>
                    <li> {{ __('home.li1') }}</li>
                    <li> {{ __('home.li2') }}</li>
                    <li> {{ __('home.li3') }}</li>
                    <li> {{ __('home.li4') }}</li>
                </ul>
                <p class="text-center">
                    {{ __('home.p4') }}
                </p>
                <p class="text-center">
                    {{ __('home.p5') }}
                </p>
            </section>
            <section>
                <p class="we">
                    {{ __('home.p6') }}
                </p>
            </section>
        </div>
        <hr>
    </div>
        <div class="d-flex justify-content-center w-100 mb-5">
            <div id="products" class="w-100 text-center">
                <div class="product-wrapper-header w-100">
                    <div class="product-header-hr">
                    </div>
                    <div class="product-header-div font-weight-bold">
                        Продукти
                    </div>
                </div>
                <div class="d-flex justify-content-between slider-wrapper">
                    <button class="btn-slider btn-prev" onclick="prev()"><</button>
                    <div class="d-flex justify-content-center slider-product-wrapper">
                        @foreach($top as $product)
                            <div class="text-center text-decoration-none sliders is-visible my-auto">
                                <a class="product-a" href="/{{App::getLocale()}}/products/{{$product->slug}}" style="text-decoration: none">
                                    <img class="slider-product-image" src="{{$product->image}}" alt="" id="{{$loop->index}}">
                                    <br>
{{--                                    <span class="justify-content-center mx-auto home-prices">--}}
{{--                                        {{$product->price}} {{__('nav.cart-price')}}--}}
{{--                                    </span>--}}
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <button class="btn-slider btn-next" id="btn-next" onclick="next()">></button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts')
    <script src="{{ asset('js/slider.js') }}"></script>
@endsection
