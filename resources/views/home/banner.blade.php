<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @include('../partials.fonts')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/banner.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/products.css') }}" rel="stylesheet">
    <link href="{{ asset('css/flowers.css') }}" rel="stylesheet">
    <link href="{{ asset('css/category.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cart.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="nav-banner">
        <div class="banner-navbar">
            <nav class="navbar navbar-expand-md shadow-sm">
                <div class="container">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false"
                            aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul id="banner-ul" class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link nav-a" href="/">
                                    {{__('nav.about')}}
                                </a>
                            </li>
                            <li id="dropdown-nav" class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{__('nav.products')}} <span class="caret"></span>
                                </a>
                                <div id="nav-dropdown-div" class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="/{{App::getLocale()}}/category/index/slug">
                                            Category
                                        </a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-a" href="/about/">
                                    {{__('nav.contacts')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="banner-right-section">
                    <div class="banner-cart-wrapper">
                        <a href="/cart">
                            <img src="/storage/style/cart.png" alt="" class="banner-image-cart">
                        </a>
                        <div class="banner-image-cart-span">
                            <span>12</span>
                        </div>
                    </div>
                    <div class="row banner-span-wrapper">
                        <div class="col-sm-10 banner-cart-div">
                            <div class="banner-cart-orders">{{__('nav.cart-your')}}</div>
                        </div>
                        <div class="col-sm-10 banner-cart-div">
                            <div class="banner-cart-orders">{{__('nav.cart-order')}}</div>
                        </div>
                        <div class="col-sm-10 banner-cart-div">
                            <div class="banner-cart-value">00.00 {{__('nav.cart-price')}}</div>
                        </div>
                    </div>
                    <div class="banner-search-wrapper">
                        <a href="" class="banner-search-wrapper">
                            <img src="/storage/style/search.png" alt="" class="banner-image-search">
                        </a>
                    </div>
                </div>
            </nav>
{{--            <div id="banner-right-section">--}}
{{--                <div class="banner-cart-wrapper">--}}
{{--                    <a href="/cart">--}}
{{--                        <img src="/storage/style/cart.png" alt="" class="banner-image-cart">--}}
{{--                    </a>--}}
{{--                    <div class="banner-image-cart-span">--}}
{{--                        <span>12</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row banner-span-wrapper">--}}
{{--                    <div class="col-sm-10 banner-cart-div">--}}
{{--                        <div class="banner-cart-orders">{{__('nav.cart-your')}}</div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-10 banner-cart-div">--}}
{{--                        <div class="banner-cart-orders">{{__('nav.cart-order')}}</div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-10 banner-cart-div">--}}
{{--                        <div class="banner-cart-value">00.00 {{__('nav.cart-price')}}</div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="banner-search-wrapper">--}}
{{--                    <a href="" class="banner-search-wrapper">--}}
{{--                        <img src="/storage/style/search.png" alt="" class="banner-image-search">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        <img class="banner-image-1" src="/storage/banners/png-1.png">
        <img class="banner-image-2" src="/storage/banners/png-2.png">
        <img class="banner-image-3" src="/storage/banners/png-3.png">
        <span class="banner-text">
            Let’s make beautiful flowers part of your life
        </span>
    </div>
</div>
</body>
</html>
