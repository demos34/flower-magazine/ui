@extends('layouts.app')

@section('title')

@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="container" id="cart">
        <div id="cart" class="cart">
            <div class="cart-header">
                <span>
                    вашата количка
                </span>
            </div>
            <div class="row mx-auto" id="cart-body">
                <div class="col-md-3 my-auto cart-item-col">
                    <img class="cart-image" src="/storage/products/product-two.png" alt="">
                </div>
                <div class="col-md-5 my-auto cart-item-col">
                    <span>
                        Бяла орхидея
                    </span>
                </div>
                <div class="col-md-2 my-auto cart-item-col">
                    <div class="cart-input">
                        <button type="button">-</button><input type="text" value="1"><button type="button">+</button>
                    </div>
                </div>
                <div class="col-md-2 my-auto cart-item-col">
                    <span id="cart-price">15.00 лв</span>
                </div>
                <div class="col-md-3 my-auto cart-item-col">
                    <img class="cart-image" src="/storage/products/product-one.png" alt="">
                </div>
                <div class="col-md-5 my-auto cart-item-col">
                    <span>
                        Бяла орхидея
                    </span>
                </div>
                <div class="col-md-2 my-auto cart-item-col">
                    <div class="cart-input">
                        <button type="button">-</button><input type="text" value="1"><button type="button">+</button>
                    </div>
                </div>
                <div class="col-md-2 my-auto cart-item-col">
                    <span id="cart-price">15.00 лв</span>
                </div>
                <div class="col-md-3 my-auto cart-item-col">
                    <img class="cart-image" src="/storage/products/product-three.png" alt="">
                </div>
                <div class="col-md-5 my-auto cart-item-col">
                    <span>
                        Бяла орхидея
                    </span>
                </div>
                <div class="col-md-2 my-auto cart-item-col">
                    <div class="cart-input">
                        <button type="button">-</button><input type="text" value="1"><button type="button">+</button>
                    </div>
                </div>
                <div class="col-md-2 my-auto cart-item-col">
                    <span id="cart-price">15.00 лв</span>
                </div>
                <div class="col-md-3 my-auto cart-item-col">
                    <img class="cart-image" src="/storage/products/product-four.png" alt="">
                </div>
                <div class="col-md-5 my-auto cart-item-col">
                    <span>
                        Бяла орхидея
                    </span>
                </div>
                <div class="col-md-2 my-auto cart-item-col">
                    <div class="cart-input">
                        <button type="button cart-item-col">-</button><input type="text" value="1"><button type="button">+</button>
                    </div>
                </div>
                <div class="col-md-2 my-auto cart-item-col">
                    <span id="cart-price">15.00 лв</span>
                </div>
            </div>
            <div class="sum-price">
                <strong>обща сума: </strong><span>60 лв</span>
            </div>
            <div class="btn-cart-wrapper">
                <button type="submit" class="btn btn-cart">поръчай</button>
            </div>
        </div>
    </div>
@endsection
