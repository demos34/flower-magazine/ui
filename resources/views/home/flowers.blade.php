@extends('layouts.app')

@section('title')

@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="container" id="wrapper">
        <div style="background-image: url('/storage/style/gif-proba.mp4'); width: 30em; height: 10em">

            <video autoplay loop muted inline>
                <source src='/storage/style/anim-kraen.mov' type="video/mov">
                <source src='/storage/style/anim-kraen.mov' type="video/mov">
                <img src='/storage/style/gif-proba.gif' />
            </video>
{{--            <video src="/storage/style/gif-proba.gif" alt="">--}}
{{--            <video width="400" controls autoplay>--}}
{{--                <source src="/storage/style/anim-kraen.mov" type="video/mp4">--}}
{{--            </video>--}}
        </div>
        <div id="products" class="flowers">
            <div class="product-wrapper-header">
                <div class="product-header-hr">
                </div>
                <div class="product-header-div font-weight-bold">
                    Цветя
                </div>
            </div>
            <div id="flowers" class="row">
                <div class="col-lg-5  m-2">
                    <a href="/category">
                        <div class="flower-wrapper my-4">
                            <img class="product-image" src="/storage/flowers/flowers-orchid.png" alt="">
                        <div id="flower-name">
                            <span>Орхидеи</span>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-5 m-2">
                    <a href="">
                        <div class="flower-wrapper my-4">
                        <img class="product-image" src="/storage/flowers/flowers-rose.jpg" alt="">
                        <div id="flower-name">
                            <span>Рози</span>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-5  m-2 my-4">
                    <a href="">
                        <div class="flower-wrapper">
                            <img class="product-image" src="/storage/flowers/flowers-others-one.jpg" alt="">
                        <div id="flower-name">
                            <span>Други</span>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="col-lg-5 mx-2 my-4">
                    <a href="">
                        <div class="flower-wrapper">
                        <img class="product-image" src="/storage/flowers/flowers-others-two.jpg" alt="">
                        <div id="flower-name">
                            <span>Други</span>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
