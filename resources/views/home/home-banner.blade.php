@extends('layouts.app')

@section('title')
    <title>Leya | Beautiful flowers for You!</title>
@endsection

@section('custom-css')

@endsection

@section('content')
    <div id="banner-home">
        <img class="banner-image-1" src="/storage/banners/png-1.png" alt="">
        <img class="banner-image-2" src="/storage/banners/png-2.png" alt="">
        <img class="banner-image-3" src="/storage/banners/png-3.png" alt="">
        <span class="banner-text">
            Let’s make beautiful flowers part of your life
        </span>
    </div>
    <div class="container" id="wrapper">
        <div class="d-flex justify-content-center">
            <img class="home-image" src="/storage/style/nav-image.png" alt="">
        </div>
        <div id="content-wrapper" class="font-weight-bold">
            <section class="home-para">
                <p class="text-center">
                    {{ __('home.p1') }}
                </p>
                <p class="text-center">
                    {{ __('home.p2') }}
                </p>

                <p class="text-center">
                    {{ __('home.p3') }}
                </p>
                <ul>
                    <li> {{ __('home.li1') }}</li>
                    <li> {{ __('home.li2') }}</li>
                    <li> {{ __('home.li3') }}</li>
                    <li> {{ __('home.li4') }}</li>
                </ul>
                <p class="text-center">
                    {{ __('home.p4') }}
                </p>
                <p class="text-center">
                    {{ __('home.p5') }}
                </p>
            </section>
            <section>
                <p class="we">
                    {{ __('home.p6') }}
                </p>
            </section>
        </div>
        <hr>
        <div id="products">
            <div class="product-wrapper-header">
                <div class="product-header-hr">
                </div>
                <div class="product-header-div font-weight-bold">
                    Продукти
                </div>
            </div>
            <div class="row product-wrapper justify-content-between" id="home-product-top">
                @foreach($top as $product)
                    <div class="col-lg-4 text-center text-decoration-none">
                        <a href="/{{App::getLocale()}}/products/{{$product->slug}}" class="product-a">
                            <img class="flower-product-image" src="{{$product->image}}" alt="">
                            <br>
                            <span class="justify-content-center mx-auto">
                                {{$product->price}} {{__('nav.cart-price')}}
                            </span>
                        </a>
                    </div>
                @endforeach
            </div>
            <hr class="product-hr">
            <div class="row product-wrapper justify-content-between" id="home-product-bot">
                @foreach($bottom as $product)
                    <div class="col-lg-4 text-center text-decoration-none">
                        <a href="/{{App::getLocale()}}/products/{{$product->slug}}" class="product-a">
                            <img class="flower-product-image" src="{{$product->image}}" alt="">
                            <br>
                            <span class="justify-content-center mx-auto">
                                {{$product->price}} {{__('nav.cart-price')}}
                            </span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
