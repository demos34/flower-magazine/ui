@extends('layouts.app')

@section('title')
    Leya | {{__('home.title')}}
@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="animated">
{{--        <video autoplay="autoplay" muted loop id="animated" playsinline>--}}
{{--            <source src="storage/style/prob.mp4" type="video/mp4">--}}
{{--        </video>--}}
        <video autoplay="autoplay" muted loop id="animated" playsinline>
            <source src="storage/style/home-video.mp4" type="video/mp4">
        </video>
    </div>
    <div class="container" id="wrapper">
        <div class="d-flex justify-content-center">
            <img class="home-image" src="/storage/style/banner-black.png" alt="">
        </div>
        <div id="content-wrapper" class="font-weight-bold">
            <section class="d-flex justify-content-center">
                <div class="w-25 mr-5">
                    <img class="rounded-text-image" src="/storage/style/text-rounded-flowers.png" alt="">
                </div>
                <div class="w-75 rounded-text-text text-justify">
                    <div id="rounded-text-para">
                        <p class="we">
                            {{ __('home.p6') }}
                        </p>
                        <p class="text-center">
                            {{ __('home.p1') }}
                        </p>
                        <input id="hidden-rounded-btn" type="submit" value="{{__('home.btn-show')}}" class="btn btn-dark" onclick="show()"/>
                    </div>
                </div>
            </section>
            <section>
                <div id="visible-on-mobile">
                        <p class="text-center">
                            {{ __('home.p2') }}
                        </p>

                        <p class="text-center">
                            {{ __('home.p3') }}
                        </p>
                        <ul>
                            <li> {{ __('home.li1') }}</li>
                            <li> {{ __('home.li2') }}</li>
                            <li> {{ __('home.li3') }}</li>
                            <li> {{ __('home.li4') }}</li>
                        </ul>
                        <p class="text-center">
                            {{ __('home.p4') }}
                        </p>
                        <p class="text-center">
                            {{ __('home.p5') }}
                        </p>
                    <input id="btn-in-hidden-div" type="submit" value="{{__('home.btn-hide')}}" class="btn btn-dark" onclick="hide()"/>
                </div>
            </section>
        </div>
    </div>
    <div id="contact-wrapper">
        <div class="w-100">
            <section class="w-100">
                <div class="send-us-msg-image text-center" style="background: url('/storage/style/send-us-msg.png')">
{{--                    <img class="send-us-msg-image" src="/storage/style/send-us-msg.png" alt="">--}}
                    <div class="send-us-msg-wrapper text-center">
                        <p class="msg-title">
                            {{__('home.send-us-msg-title')}}
                        </p>
                        <p class="msg-body" id="desc-contact">
                            {{__('home.send-us-msg-body')}}
                        </p>
                        <p class="msg-body" id="mobile-one-contact">
                            {{__('home.send-us-msg-body-1')}}
                        </p>
                        <p class="msg-body" id="mobile-two-contact">
                            {{__('home.send-us-msg-body-2')}}
                        </p>
                        <div class="send-us-form-wrapper">
                            <form class="send-us-msg-form" action="{{route('message')}}" method="post">
                                @csrf
                                <div class="my-auto">
                                    <input
                                        class="@error('email') is-invalid @enderror"
                                        id="email"
                                        name="email"
                                        type="email"
                                        placeholder="email..."
                                        value="{{ old('email') }}"
                                        required autocomplete="phone">
                                    <button class="send-us-msg-btn">{{__('home.send-us-msg-btn')}}</button>
                                    @error('email')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <br>
                                <textarea class="@error('msg') is-invalid @enderror"
                                          id="msg"
                                          name="msg"
                                          type="msg"
                                          required autocomplete="msg">{{ old('email') }}</textarea>
                                @error('msg')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </form>
                        </div>
                    </div>
                    <div class="contact-us-wrapper">
                        <div class="d-flex justify-content-between">
                            <div>
                                <a href="">
                                    <img class="contact-images" src="/storage/style/phone.png" alt="phone">
                                </a><span class="contact-text">+359888 888 888</span>
                            </div>
                            <div>
                                <a href="">
                                    <img class="contact-images" src="/storage/style/insta.png" alt="phone">
                                </a> <span class="contact-text">+359888 888 888</span>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div>
                                <a href="">
                                    <img class="contact-images" src="/storage/style/email.png" alt="phone">
                                </a><span class="contact-text">+359888 888 888</span>
                            </div>
                            <div>
                                <a href="">
                                    <img class="contact-images" src="/storage/style/facebook.png" alt="phone">
                                </a><span class="contact-text">+359888 888 888</span>
                            </div>
                        </div>
                    </div>
                    <div class="my-auto contact-us-strong">
                        <strong>
                            {{__('home.contact-us')}}
                        </strong>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div>
        <div class="d-flex justify-content-center w-100 mb-5">
            <div id="products" class="w-100 text-center">
                <div class="product-wrapper-header w-100">
                    <div class="product-header-hr">
                    </div>
                    <div class="product-header-div font-weight-bold">
                        {{__('home.title-products')}}
                    </div>
                </div>
                <div class="d-flex justify-content-between slider-wrapper">
                    <button class="btn-slider btn-prev" onclick="prev()">
                        <img class="left-btn-arrow" src="storage/style/left.png" alt="">
                    </button>
                    <div class="d-flex justify-content-center slider-product-wrapper">
                        @foreach($top as $product)
                            <div class="text-center text-decoration-none sliders is-visible my-auto">
                                <a class="product-a" href="/{{App::getLocale()}}/products/{{$product->slug}}" style="text-decoration: none">
                                    <img class="slider-product-image" src="{{$product->image}}" alt="" id="{{$loop->index}}">
{{--                                    <br>--}}
{{--                                    <span class="justify-content-center mx-auto home-prices">--}}
{{--                                        {{$product->price}} {{__('nav.cart-price')}}--}}
{{--                                    </span>--}}
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <button class="btn-slider btn-next" id="btn-next" onclick="next()">
                        <img class="right-btn-arrow" src="storage/style/left.png" alt="">
                    </button>
                </div>
                <div class="d-flex justify-content-between car-wrapper-two">
                    <button class="btn-car-two btn-car-prev-two" onclick="pTwo()">
                        <img class="left-btn-arrow" src="storage/style/left.png" alt="">
                    </button>
                    <div class="d-flex justify-content-center car-product-wrapper-two">
                        @foreach($bottom as $product)
                            <div class="text-center text-decoration-none car-two is-display-two my-auto">
                                <a class="product-a" href="/{{App::getLocale()}}/products/{{$product->slug}}" style="text-decoration: none">
                                    <img class="car-product-image-two" src="{{$product->image}}" alt="" id="{{$loop->index}}-two">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <button class="btn-car-two btn-car-next-two" id="btn-car-next-two" onclick="nTwo()">
                        <img class="right-btn-arrow" src="storage/style/left.png" alt="">
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('custom-scripts')
    <script src="{{ asset('js/slider.js') }}"></script>
    <script src="{{ asset('js/slider-two.js') }}"></script>
    <script src="{{ asset('js/show-hide-home-info.js') }}"></script>
{{--    <script type="text/javascript">--}}
{{--        var $video  = $('video'),--}}
{{--            $window = $(window);--}}

{{--        $(window).resize(function(){--}}
{{--            var height = $window.height();--}}
{{--            $video.css('height', height);--}}

{{--            var videoWidth = $video.width(),--}}
{{--                windowWidth = $window.width(),--}}
{{--                marginLeftAdjust =   (windowWidth - videoWidth) / 2;--}}

{{--            $video.css({--}}
{{--                'height': height,--}}
{{--                'marginLeft' : marginLeftAdjust--}}
{{--            });--}}
{{--        }).resize();--}}
{{--    </script>--}}
@endsection
