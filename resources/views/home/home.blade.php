@extends('layouts.app')

@section('title')
    Leya | {{__('home.title')}}
@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="animated">
        <video autoplay="autoplay" muted loop id="animated" playsinline>
            <source src="storage/style/home-video.mp4" type="video/mp4">
        </video>
    </div>
    <div class="container" id="wrapper">
        <div class="d-flex justify-content-center">
            <img class="home-image" src="/storage/style/banner-black.png" alt="">
        </div>
        <div id="content-wrapper" class="font-weight-bold">
            <section class="d-flex justify-content-center">
                <div class="w-25 mr-5">
                    <img class="rounded-text-image" src="/storage/style/text-rounded-flowers.png" alt="">
                </div>
                <div class="w-75 rounded-text-text text-justify">
                    <div id="rounded-text-para">
                        <p class="we">
                            {{ __('home.p6') }}
                        </p>
                        <p class="text-center">
                            {{ __('home.p1') }}
                        </p>
                        <input id="hidden-rounded-btn" type="submit" value="{{__('home.btn-show')}}" class="btn btn-dark" onclick="show()"/>
                    </div>
                </div>
            </section>
            <section>
                <div id="visible-on-mobile">
                        <p class="text-center">
                            {{ __('home.p2') }}
                        </p>

                        <p class="text-center">
                            {{ __('home.p3') }}
                        </p>
                        <ul>
                            <li> {{ __('home.li1') }}</li>
                            <li> {{ __('home.li2') }}</li>
                            <li> {{ __('home.li3') }}</li>
                            <li> {{ __('home.li4') }}</li>
                        </ul>
                        <p class="text-center">
                            {{ __('home.p4') }}
                        </p>
                        <p class="text-center">
                            {{ __('home.p5') }}
                        </p>
                    <input id="btn-in-hidden-div" type="submit" value="{{__('home.btn-hide')}}" class="btn btn-dark" onclick="hide()"/>
                </div>
            </section>
        </div>
        <div class="d-flex justify-content-center w-100 my-5 mx-auto p-0">
            <div id="products" class="w-100 text-center mt-5 mx-auto">
                <div class="product-wrapper-header w-100">
                    <div class="product-header-hr" id="hr">
                    </div>
                    <div class="product-header-div font-weight-bold" id="text">
                        {{__('home.title-products')}}
                    </div>
                </div>
                <div class="d-flex justify-content-center slider-wrapper mx-auto w-100">
                    <button class="btn-slider btn-prev" onclick="prev()">
                        <img class="left-btn-arrow" src="storage/style/left.png" alt="">
                    </button>
                    <div class="d-flex justify-content-center slider-product-wrapper">
                        @foreach($top as $product)
                            <div class="text-center text-decoration-none sliders is-visible my-auto">
                                <a class="product-a" href="/{{App::getLocale()}}/products/{{$product->slug}}" style="text-decoration: none">
                                    <img class="slider-product-image" src="{{$product->image}}" alt="" id="{{$loop->index}}">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <button class="btn-slider btn-next" id="btn-next" onclick="next()">
                        <img class="right-btn-arrow" src="storage/style/left.png" alt="">
                    </button>
                </div>

                <div class="d-flex justify-content-between car-wrapper-two w-100">
                    <button class="btn-car-two btn-car-prev-two" onclick="pTwo()">
                        <img class="left-btn-arrow" src="storage/style/left.png" alt="">
                    </button>
                    <div class="d-flex justify-content-center car-product-wrapper-two">
                        @foreach($bottom as $product)
                            <div class="text-center text-decoration-none car-two is-display-two my-auto">
                                <a class="product-a" href="/{{App::getLocale()}}/products/{{$product->slug}}" style="text-decoration: none">
                                    <img class="car-product-image-two" src="{{$product->image}}" alt="" id="{{$loop->index}}-two">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <button class="btn-car-two btn-car-next-two" id="btn-car-next-two" onclick="nTwo()">
                        <img class="right-btn-arrow" src="storage/style/left.png" alt="">
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('custom-scripts')
    <script src="{{ asset('js/slider.js') }}"></script>
    <script src="{{ asset('js/slider-two.js') }}"></script>
    <script src="{{ asset('js/show-hide-home-info.js') }}"></script>
    <script src="{{ asset('js/lines.js') }}"></script>
@endsection
