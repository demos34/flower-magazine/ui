@extends('layouts.app')

@section('title')

@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="container" id="category-wrapper">
        <div id="products" class="flowers">
            <div class="product-wrapper-header">
                <div class="product-header-hr">
                </div>
                <div class="product-header-div font-weight-bold">
                    Орхидеи
                </div>
                <div class="select-div text-center">
                    <select class="classic">
                        <option>СОРТИРАНЕ</option>
                        <option>По цена (възходящо)</option>
                        <option>По цена (низходящо)</option>
                        <option>От стари към нови</option>
                        <option>От нови към стари</option>
                    </select>
                </div>
                <div class="flowers-colors" id="mobile">
                    <label>
                        цвят
                    </label>
                    <div class="row my-1" id="color-wrapper">
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/blue.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/blue-one.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/blue-two.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/red.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/red-one.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/red-two.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/white.png" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/white-one.png" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/white-two.png" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/blue.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/blue-one.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/blue-two.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/red.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/red-one.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/red-two.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/white.png" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/white-one.png" alt="">
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="/color">
                                <img class="color-image rounded-circle" src="/storage/colors/white-two.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="my-5" id="content-wrapper">
                    <div class="flowers-colors" id="desk">
                        <label>
                            цвят
                        </label>
                        <div class="row my-1" id="color-wrapper">
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/blue.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/blue-one.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/blue-two.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/red.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/red-one.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/red-two.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/white.png" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/white-one.png" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/white-two.png" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/blue.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/blue-one.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/blue-two.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/red.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/red-one.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/red-two.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/white.png" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/white-one.png" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <a href="/color">
                                    <img class="color-image rounded-circle" src="/storage/colors/white-two.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="flowers-products">
                        <div class="row " id="flowers-product-wrapper">
                            <div class="col-lg-4 flower-item-col" id="flower-item-wrapper">
                                <a href="/products">
                                    <img class="flower-product-image" src="/storage/products/product-one.png" alt="">
                                    <span>
                                        15.00 лв.
                                    </span>
                                </a>
                            </div>
                            <div class="col-lg-4 flower-item-col flower-item-col" id="flower-item-wrapper">
                                <a href="/products">
                                    <img class="flower-product-image" src="/storage/products/product-two.png" alt="">
                                    <span>
                                        15.00 лв.
                                    </span>
                                </a>
                            </div>
                            <div class="col-lg-4 flower-item-col" id="flower-item-wrapper">
                                <a href="/products">
                                    <img class="flower-product-image" src="/storage/products/product-three.png" alt="">
                                    <span>
                                        15.00 лв.
                                    </span>
                                </a>
                            </div>
                            <div class="col-lg-4 flower-item-col" id="flower-item-wrapper">
                                <a href="/products">
                                    <img class="flower-product-image" src="/storage/products/product-four.png" alt="">
                                    <span>
                                        15.00 лв.
                                    </span>
                                </a>
                            </div>
                            <div class="col-lg-4 flower-item-col" id="flower-item-wrapper">
                                <a href="/products">
                                    <img class="flower-product-image" src="/storage/products/product-five.png" alt="">
                                    <span>
                                        15.00 лв.
                                    </span>
                                </a>
                            </div>
                            <div class="col-lg-4 flower-item-col" id="flower-item-wrapper">
                                <a href="/products">
                                    <img class="flower-product-image" src="/storage/products/product-six.jpg" alt="">
                                    <span>
                                        15.00 лв.
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
