<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

{{--    <link rel="icon" href="/storage/style/nav-image.ico" type="image/x-icon">--}}
{{--    <link rel="shortcut icon" href="/storage/style/nav-image.ico" type="image/x-icon">--}}

{{--    <meta name="title" property="og:title" content="leya" >--}}
{{--    <meta property="og:description" content="Desc">--}}
    <meta name="image" property="og:image" content="https://leya.isoftd.com/favicon.ico">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
{{--    <link rel="icon" href="/storage/style/nav-image.ico" type="image/x-icon">--}}
{{--    <link rel="shortcut icon" href="/storage/style/nav-image.ico" type="image/x-icon">--}}
    <title>
        @yield('title')
    </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    @include('partials.fonts')
    @yield('meta')

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @include('partials.custom-css')
    @yield('custom-css')
</head>
<body>
<div id="app">
    @include('partials.nav')
    @include('partials.alerts')
{{--    @include('partials.navbar-two')--}}
    <main class="py-2 main">
        @yield('content')
        @yield('custom-scripts')
    </main>
</div>
@include('partials.footer')
@yield('custom-scripts')
<script src="{{ asset('js/show-hide-footer.js') }}"></script>
</body>
</html>
