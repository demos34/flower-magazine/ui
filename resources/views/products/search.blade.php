@extends('layouts.app')

@section('title')
    Leya | Search
@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="container" id="search-wrapper">
        <div id="search" class="flowers">
            <div class="products-search">
                <div id="content-wrapper">
                    <div class="flowers-products">
                        <div class="row " id="flowers-product-wrapper">
                            @foreach($products as $product)
                                <div class="col-lg-4 flower-item-col" id="flower-item-wrapper">
                                    <a href="/{{App::getLocale()}}/products/{{$product->slug}}">
                                        <img class="flower-product-image" src="{{$product->image}}" alt="">
                                        <br>
                                        <span class="justify-content-center ml-2">
                                        {{$product->name}}
                                        </span>
                                        <br>
                                        <span class="justify-content-center ml-2">
                                        {{$product->price}} {{__('nav.cart-price')}}
                                        </span>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts')
    <script src="{{ asset('js/lines.js') }}"></script>
@endsection
