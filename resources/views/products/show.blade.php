@extends('layouts.app')

@section('title')
    Leya | Show {{$product->name}}
@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="container" id="product-wrapper">
        <div id="product-row-wrapper" class="row" style="">
            <div class="col-lg-4 my-auto">
                <div class="well my-2 justify-content-center" id="image">
                    <img class="product-image-show" src="{{$product->image}}" alt="" style="" id="showed_image">
                </div>
                <div class="justify-content-center">
                    <span class="secondary-image">
                        <img src="{{$product->image}}" style="" alt="" onclick="func(this.src)">
                    </span>
                    @foreach($product->images as $image)
                        <span class="secondary-image">
                            <img src="{{$image->image}}" alt="" onclick="func(this.src)">
                        </span>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-8 my-1 p-1">
                <div class="d-flex justify-content-center">
                    <div class="text-center product-name-show" style="">
                        <h1>
                            {{$product->name}}
                        </h1>
                        <p>
                            @if($product->warehouse->quantity == 0)
                                <strong class="in-stock-alert">{{__('product.alert')}}</strong>
                            @else
                                <strong class="in-stock-success">{{__('product.success')}}</strong>
                            @endif
                        </p>
                    </div>
                </div>
                <form action="/cart/{{$product->id}}" method="POST">
                    @csrf
                    <div class="d-flex justify-content-between">
                        <div class="">
                            <p>
                                <span id="product-size-show">
                                    {{__('product.size')}} {{$product->size}} cm
                                </span>
                            </p>
                            <p>
                                <span id="product-color-show">
                                    {{__('product.color')}} {{$color->color}}
                                </span>
                            </p>
                            <p>
                                <span>
                                    <span onclick="show()" class="description" id="product-description-show">{{__('product.more-description')}}</span>
                                </span>
                            </p>
                        </div>
                        <div class="w-50">
                                <h2>
                                    <input disabled hidden id="input-price" value="{{$product->price}}">
                                    <span id="product-price-show" class="pr-5">
                                            {{$product->price}} {{__('nav.cart-price')}}
                                    </span>
                                </h2>
                                <div class="cart-show-input">
                                    <button class="qtyMinus" type="button" onclick="minus({{$product->id}})">-</button>
                                        <input type="text" class="qtyField" name="quantity" value="1" min="1" id="input-quantity">
                                    <button class="qtyPlus" type="button" onclick="plus({{$product->id}})">+</button>
                                </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        @if($product->warehouse->quantity == 0)
                            <strong class="in-stock-alert">{{__('product.out')}}</strong>
                        @else
                            <button id="add-to-cart" class="btn w-50 font-weight-bolder">{{__('product.button')}}
                            </button>
                        @endif
                    </div>
                </form>
            </div>
            <div class="col-md-10 text-center hidden-product-description float-right" id="show">
                <p>
                    {{$product->description}}
                </p>
                <p>
                    <span onclick="hide()" class="description" >{{__('product.less-description')}}</span>
                </p>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')
    <script src="{{ asset('js/change-src.js') }}"></script>
    <script src="{{ asset('js/show-hide-description.js') }}"></script>
    <script src="{{ asset('js/change-quantity-product.js') }}"></script>
@endsection

