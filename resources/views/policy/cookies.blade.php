@extends('layouts.app')

@section('title')
    Leya | Policy - Cookies
@endsection

@section('custom-css')
    <link href="{{ asset('css/policy.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container" id="policy">
        <div class="m-5">
            <h2>
                {{__('policy-cookies.h2')}}
            </h2>
            <p>
                {{__('policy-cookies.p-one')}}
            </p>
            <p>
                {{__('policy-cookies.p-two')}}
            </p>
            <p>
                {{__('policy-cookies.p-three')}}
            </p>
            <p>
                {{__('policy-cookies.p-four')}}
            </p>
            <p>
                {{__('policy-cookies.p-five')}}
            </p>
            <h3>
                {{__('policy-cookies.h3-one')}}
            </h3>
            <p>
                {{__('policy-cookies.p-six')}}
            </p>
            <ul>
                <li>
                    {{__('policy-cookies.li-one')}}
                </li>
                <li>
                    {{__('policy-cookies.li-two')}}
                </li>
                <li>
                    {{__('policy-cookies.li-three')}}
                </li>
                <li>
                    {{__('policy-cookies.li-four')}}
                </li>
                <li>
                    {{__('policy-cookies.li-five')}}
                </li>
                <li>
                    {{__('policy-cookies.li-six')}}
                </li>
            </ul>
            <p>
                    {{__('policy-cookies.p-seven')}}
            </p>
            <h3>
                    {{__('policy-cookies.h3-two')}}
            </h3>
            <p>
                    {{__('policy-cookies.p-eight')}}
            </p>
            <p>
                    {{__('policy-cookies.p-nine')}}
            </p>
        </div>
    </div>
@endsection
