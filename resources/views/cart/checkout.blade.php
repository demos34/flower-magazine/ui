@extends('layouts.app')

@section('title')
    Leya | {{__('your-cart.title-summary')}}
@endsection

@section('content')
    <div class="container" id="checkout">
        <div id="cart" class="cart">
            <div class="cart-header">
                <span>
                    {{__('your-cart.your-cart')}}
                </span>
            </div>
        </div>
        <div class="d-flex justify-content-center my-2">
            <div class="sum-price">
                <strong>{{__('your-cart.sum')}}: </strong><span id="sum">{{$total}}</span> <span>{{__('nav.cart-price')}}</span>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div class="row justify-content-center" id="form-checkout-wrapper">
                <form class="purchase-checkout" action="/purchase" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="col-md-10 my-3 purchase-orders">
                        <label for="names" class="form-check-label">
                            {{__('checkout.names')}}:
                        </label>
                        <input
                            id="names"
                            name="names"
                            class="form-control @error('names') is-invalid @enderror" type="text" style="width: 25em"
                            value="{{ old('names') }}">
                        @error('names')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-10 my-3 purchase-orders">
                        <label for="phone" class="form-check-label">
                            {{__('checkout.phone')}}:
                        </label>
                        <input
                            id="phone"
                            name="phone"
                            class="form-control @error('phone') is-invalid @enderror" type="text" style="width: 25em"
                            value="{{ old('phone') }}"
                            required autocomplete="phone" autofocus>
                        @error('phone')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-10 my-3 purchase-orders">
                        <label for="address" class="form-check-label">
                            {{__('checkout.address')}}:
                        </label>
                        <input
                            id="address"
                            name="address"
                            class="form-control @error('address') is-invalid @enderror" type="text" style="width: 25em"
                            value="{{ old('address') }}"
                            required autocomplete="address" autofocus>
                        @error('address')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-10 my-3 purchase-orders">
                        <label for="email" class="form-check-label">
                            {{__('checkout.email')}}:
                        </label>
                        <input
                            id="email"
                            name="email"
                            class="form-control @error('email') is-invalid @enderror" type="email" style="width: 25em"
                            value="{{ old('email') }}"
                            required autocomplete="email" autofocus>
                        @error('email')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-10 my-3 purchase-orders">
                        <label for="additional_info" class="form-check-label">
                            {{__('checkout.additional')}}:
                        </label>
                        <textarea
                            id="additional_info"
                            name="additional_info"
                            placeholder="{{__('checkout.textarea-placeholder')}}"
                            style="height: 10em; width: 25em"
                            class="form-control @error('additional_info') is-invalid @enderror" type="additional_info" style="width: 25em"
                            required autocomplete="email" autofocus>{{ old('additional_info') }}</textarea>
                        @error('additional_info')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="btn-cart-wrapper">
                            <button type="submit" class="btn btn-cart">{{__('your-cart.end')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
