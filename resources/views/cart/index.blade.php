@extends('layouts.app')

@section('title')
    Leya | {{__('your-cart.title')}}
@endsection

@section('custom-css')

@endsection

@section('content')
    <div class="w-100">
        <div class="cart-header">
            <span>
                {{__('your-cart.your-cart')}}
            </span>
        </div>
    </div>
    <div class="container" id="cart">
        <div class="cart">
            <form action="/cart" method="POST">
                @csrf
                @method('PUT')
                <div class="row mx-auto" id="cart-body">
                    @foreach($cart as $item)
                        <div class="col-md-3 my-auto cart-item-col">
                            <img class="cart-image" src="{{$item->options->image}}" alt="">
                        </div>
                        <div class="col-md-5 my-auto cart-item-col">
                        <span>
                            {{$item->name}}
                        </span>
                        </div>
                        <div class="col-md-2 my-auto cart-item-col">
                            <div class="cart-input">
                                <button class="qtyMinus" type="button" onclick="minus({{$item->id}})">-</button>
                                 <input type="text" class="qtyField" name="quantity" value="{{$item->options->quantity}}" min="1" id="input-quantity-{{$item->id}}">
                                <button class="qtyMinus" type="button" onclick="plus({{$item->id}})">+</button>
                            </div>
                        </div>
                        <div class="col-md-2 my-auto cart-item-col">
                            <input disabled hidden id="input-price-{{$item->id}}" value="{{$item->price}}">
                            <strong class="cart-price price" id="input-total-price-{{$item->id}}">{{$item->options->full_price}}</strong><strong id="cart-price">{{__('nav.cart-price')}}</strong>
                            <a href="/cart/delete/{{$item->rowId}}" class="my-auto">
                                <button id="cart-delete-content-btn" type="button" class="btn btn-danger" onclick="return confirm('Are Your sure?')">
                                    {{__('your-cart.delete')}}
                                </button>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="sum-price">
                    <strong>{{__('your-cart.sum')}}: </strong><span id="sum">{{$total}}</span> <span>{{__('nav.cart-price')}}</span>
                </div>
                <div class="btn-cart-wrapper">
                    <button type="submit" class="btn btn-cart">{{__('your-cart.purchase')}}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('custom-scripts')
    <script src="{{ asset('js/change-quantity.js') }}"></script>
@endsection
