<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function images()
    {
        return $this->belongsToMany('App\Models\Image');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Keyword');
    }

    public function color()
    {
        return $this->belongsTo('App\Models\Color');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Item');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Models\Cart');
    }

    public function warehouse()
    {
        return $this->hasOne('\App\Models\Warehouse');
    }
}
