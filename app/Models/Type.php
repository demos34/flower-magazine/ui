<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Type extends Model
{
    public function getRouteKeyName()
    {

        $prefix = $this->getPrefix();
        return $prefix . 'slug';
    }

    protected function  getPrefix()
    {
        if(App::getLocale() == 'en'){
            $prefix = 'eng_';
        } else {
            $prefix = App::getLocale() . '_';
        }

        return $prefix;
    }
}
