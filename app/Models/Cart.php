<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];

    public function items()
    {
        return $this->belongsToMany('App\Models\Item');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    public function buyings()
    {
        return $this->hasMany('App\Models\Buying');
    }
}
