<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buying extends Model
{
    protected $guarded = [];

    public function movement()
    {
        $this->belongsTo('App\Models\Movement');
    }

    public function cart()
    {
        $this->belongsTo('App\Models\Cart');
    }
}
