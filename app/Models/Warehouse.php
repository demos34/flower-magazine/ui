<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('\App\Models\Category');
    }

    public function product()
    {
        return $this->belongsTo('\App\Models\Product');
    }

    public function type()
    {
        return $this->belongsTo('\App\Models\Type');
    }

    public function movements()
    {
        return $this->hasMany('\App\Models\Movement');
    }
}
