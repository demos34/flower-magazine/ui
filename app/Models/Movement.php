<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    public function warehouse()
    {
        return $this->belongsTo('\App\Models\Warehouse');
    }

    public function details()
    {
        return $this->hasMany('App\Models\Detail');
    }

    public function buyings()
    {
        return $this->hasMany('App\Models\Buying');
    }
}
