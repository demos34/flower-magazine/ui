<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $guarded = [];

    public function movement()
    {
        return $this->belongsTo('App\Models\Movement');
    }
}
