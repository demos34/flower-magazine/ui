<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Category extends Model
{
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Keyword');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }

    public function warehouses()
    {
        return $this->hasMany('\App\Models\Warehouse');
    }


    protected function  getPrefix()
    {
        if(App::getLocale() == 'en'){
            $prefix = 'eng_';
        } else {
            $prefix = App::getLocale() . '_';
        }

        return $prefix;
    }
}
