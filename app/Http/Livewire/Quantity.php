<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Quantity extends Component
{
    public $quantity = 1;

    public function increment()
    {
        $this->quantity++;
    }

    public function decrement()
    {

        $this->quantity--;
        if ($this->quantity - 1 < 1) {
            $this->quantity = 0;
        }
    }

    public function render()
    {
        return view('livewire.quantity');
    }
}
