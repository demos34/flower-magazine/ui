<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, CartRepositoryInterface $cartRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->cartRepository = $cartRepository;
    }

    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $cart = $this->cartRepository->getCartContent();
        $total = $this->cartRepository->getTotal();
        return view('cart.index')->with(
            [
                'cart' => $cart,
                'total' => $total,
            ]
        );
    }

    public function addToCart(Request $request, Product $product)
    {
        $action = 'addToCart';
        $this->trafficRepository->getRemoteAddress($action);

        $duplicate = Cart::search(function ($cartItem, $rowId) use ($product){
            return $cartItem->id === $product->id;
        });

        if ($duplicate->isNotEmpty()){
            return redirect('/cart')->with('error');
        }

        $cart = $this->cartRepository->addCartContent($request, $product);
        if ($cart === FALSE)
        {
            return redirect()->back()->with('alert', 'The quantity cannot be smaller than 1! Please fill again');
        }
        return redirect('/cart')->with('success', 'The item was added to your cart!');
    }


    public function updateCart(Request $request)
    {
        $action = 'updateCart';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->cartRepository->updateCart($request);

        return redirect('/checkout')->with($msg['session'], $msg['message']);
    }

    public function checkout()
    {
        $action = 'checkout';
        $this->trafficRepository->getRemoteAddress($action);

        $total = $this->cartRepository->getTotal();

        if((float)$total < 0.01){
            return redirect('/cart')->with('alert', 'You have not items in your cart! Please add something!');
        }

        return view('cart.checkout')->with('total', $total);
    }

    public function purchase(Request $request)
    {
        $action = 'purchase';
        $this->trafficRepository->getRemoteAddress($action);

        $this->cartRepository->purchase($request);
        return redirect('/cart')->with('success', 'Вашата поръчка е приета и ще се свържем с Вас!');
    }


    public function delete($rowId)
    {
        $action = 'delete';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->cartRepository->deleteFromCart($rowId);
        return redirect('/cart')->with($msg['session'], $msg['message']);
    }
}
