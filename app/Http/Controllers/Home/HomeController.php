<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ProductsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var ProductsRepositoryInterface
     */
    private $productsRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                ProductsRepositoryInterface $productsRepository,
                                CategoryRepositoryInterface $categoryRepository,
                                CartRepositoryInterface $cartRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->productsRepository = $productsRepository;
        $this->categoryRepository = $categoryRepository;
        $this->cartRepository = $cartRepository;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $topProducts = $this->productsRepository->getRandomProducts();
        $bottomProducts = $this->productsRepository->getRandomProducts();

        return view('home.home')->with(
            [
                'top' => $topProducts,
                'bottom' => $bottomProducts,
            ]
        );
    }

    public function about()
    {
        $action = 'contacts';
        $this->trafficRepository->getRemoteAddress($action);

        return view('home.contacts');
    }

    public function message(Request $request)
    {
        $action = 'message';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->cartRepository->sendUsMsg($request);
        return redirect()->back()->with('success', 'Съобщението Ви е изпратено! ЩЕ се свържем с Вас възможно най-скоро');
    }

    public function cart()
    {
        $action = 'cart';
        $this->trafficRepository->getRemoteAddress($action);
        return view('home.cart');
    }

    public function products()
    {
        return view('home.products');
    }

    public function flowers()
    {
        return view('home.flowers');
    }

    public function category()
    {
        return view('home.category');
    }

    public function search(Request $request)
    {
        $action = 'search';
        $this->trafficRepository->getRemoteAddress($action);

        dd($request->all());
    }
}
