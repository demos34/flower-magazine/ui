<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ProductsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var ProductsRepositoryInterface
     */
    private $productsRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                ProductsRepositoryInterface $productsRepository,
                                CategoryRepositoryInterface $categoryRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->productsRepository = $productsRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function show(Product $product)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        $newProduct = $this->productsRepository->getProduct($product);
        $color = $this->productsRepository->getColorOfProduct($product);


        return view('products.show')->with(
            [
                'product' => $newProduct,
                'color' => $color,
            ]
        );
    }

    public function search(Request $request)
    {
        $action = 'search';
        $this->trafficRepository->getRemoteAddress($action);

        $products = $this->productsRepository->searchForProducts($request);
        $sort = $this->categoryRepository->getAllSortBy();
        $colors = $this->categoryRepository->getAllColors();



        return view('products.search')->with(
            [
                'sort' => $sort,
                'products' => $products,
                'colors' => $colors,
            ]
        );
    }


}
