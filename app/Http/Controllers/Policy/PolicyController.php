<?php

namespace App\Http\Controllers\Policy;

use App\Http\Controllers\Controller;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class PolicyController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository)
    {

        $this->trafficRepository = $trafficRepository;
    }

    public function security()
    {
        $action = 'security';
        $this->trafficRepository->getRemoteAddress($action);

        return view('policy.security');
    }

    public function summary()
    {
        $action = 'summary';
        $this->trafficRepository->getRemoteAddress($action);

        return view('policy.summary');
    }

    public function cookies()
    {
        $action = 'cookies';
        $this->trafficRepository->getRemoteAddress($action);

        return view('policy.cookies');
    }
}
