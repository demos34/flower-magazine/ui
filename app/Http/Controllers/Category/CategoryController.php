<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Color;
use App\Models\SortItem;
use App\Models\Type;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, CategoryRepositoryInterface $categoryRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Type $type)
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();
        $newType = $this->categoryRepository->getTypes($type);
        $categories = $this->categoryRepository->getCategories($type, null, true);
        if($type->id === 3){
            $front = true;
        } else {
            $front = false;
        }

        return view('categories.index')->with(
            [
                'categories' => $categories,
                'tags' => $tags,
                'keywords' => $keywords,
                'type' => $newType,
                'front' => $front,
            ]
        );
    }

    public function sessionColor($locale, Color $color, Category $category)
    {
        $action = 'sessionColor';
        $this->trafficRepository->getRemoteAddress($action);
        Session::put('color', $color->id);

        if($locale == 'en'){
            return redirect()->route('category-show-all-en', [
                'category' => $category,
            ]);
        } else {
            return redirect()->route('category-show-all-bg', [
                'category' => $category,
            ]);
        }
//        return redirect('category/show/'.$category->id);
    }

    public function sessionSort($locale, SortItem $sortItem, Category $category)
    {
        $action = 'sessionSort';
        $this->trafficRepository->getRemoteAddress($action);

        Session::put('sort', $sortItem->id);

        if($locale == 'en'){
            return redirect()->route('category-show-all-en', [
                'category' => $category,
            ]);
        } else {
            return redirect()->route('category-show-all-bg', [
                'category' => $category,
            ]);
        }
//        return redirect('category/show/'.$category->id);
    }

    public function viewAllProducts(Category $category)
    {
        $action = 'viewAllProducts';
        $this->trafficRepository->getRemoteAddress($action);

        $sortedProducts = $this->categoryRepository->getAllProductsSortedBy($category);
        $newTypedCategory = $this->categoryRepository->getCategories($category->type, $category->id, false);
        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();
        $sort = $this->categoryRepository->getAllSortBy();
        $colors = $this->categoryRepository->getAllColors();

        return view('categories.all')->with(
            [
                'category' => $newTypedCategory,
                'tags' => $tags,
                'keywords' => $keywords,
                'sort' => $sort,
                'products' => $sortedProducts,
                'colors' => $colors,
            ]
        );
    }
}
