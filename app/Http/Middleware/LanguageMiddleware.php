<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use MongoDB\Driver\Session;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $response = $next($request);
        if(session()->has('locale')){
            app()->setLocale(session('locale'));
//            App::setLocale(session('locale'));
        }
        else {
            app()->setLocale(config('app.locale'));
        }

//        dd($next($request));

        return $next($request);
    }
}
