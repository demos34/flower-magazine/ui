<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Message;
use App\Models\Type;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
//        view()->composer('partials.navbar-two', function ($view) {
        view()->composer('partials.nav', function ($view) {

            if(App::getLocale() == 'en'){
                $lang = 'eng_';
            } else {
                $lang = App::getLocale() . '_';
            }

            $view->with
            ([
                'categories' => Category::select('id',
                    $lang . 'name as name',
                    $lang . 'slug as slug'
                ),
                'total' => Cart::subtotal(),
                'count' => Cart::count(),
                'types' => Type::select('id',
                $lang . 'name as name',
                $lang . 'slug as slug'
                )->get()

            ]);
        });
    }
}
