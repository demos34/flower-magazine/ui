<?php

namespace App\Providers;

use App\Repositories\CartRepository;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ProductsRepository;
use App\Repositories\ProductsRepositoryInterface;
use App\Repositories\TrafficRepository;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(CartRepositoryInterface::class, CartRepository::class);
        $this->app->singleton(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->singleton(ProductsRepositoryInterface::class, ProductsRepository::class);
        $this->app->singleton(TrafficRepositoryInterface::class, TrafficRepository::class);
    }
}
