<?php


namespace App\Repositories;


use App\Models\Category;
use App\Models\Color;
use App\Models\Keyword;
use App\Models\Product;
use App\Models\SortItem;
use App\Models\Tag;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function getTypes(Type $type)
    {
        return $this->getMappedTypes($type);
    }

    public function getCategories(Type $type, $categoryId, $plural)
    {
        if($plural === TRUE){
            return $this->getMappedCategories($type);
        } else {
            return $this->getMappedCategory($type, $categoryId);
        }

    }

    public function getAllTags()
    {
        return Tag::all();
    }

    public function getAllKeywords()
    {
        return Keyword::all();
    }

    public function getAllSortBy()
    {
        return $this->getMappedSortedItems();
    }

    public function getAllColors()
    {
        $prefix = $this->getPrefix();

        return Color::select('id',
            $prefix . 'color as color',
            'image'
        )->get();
    }

    public function getAllProductsSortedBy(Category $category)
    {
        $prefix = $this->getPrefix();
        $sortItem = $this->emptySessionSort();
        $color = $this->emptySessionColor();

        $this->cookie($sortItem, $color);
        if($color->id == 1){
            $product = Product::select(
                'id',
                $prefix . 'name as name',
                $prefix . 'slug as slug',
                $prefix . 'meta_description as meta_description',
                $prefix . 'description as description',
                'image', 'price', 'color_id', 'category_id', 'size'
            )->where(
                [
                    'category_id' => $category->id,
                ]
            )->orderBy($sortItem->column, $sortItem->to_where)->get();
        } else {
            $product = Product::select(
                'id',
                $prefix . 'name as name',
                $prefix . 'slug as slug',
                $prefix . 'meta_description as meta_description',
                $prefix . 'description as description',
                'image', 'price', 'color_id', 'category_id', 'size'
            )->where(
                [
                    'category_id' => $category->id,
                    'color_id' => $color->id,
                ]
            )->orderBy($sortItem->column, $sortItem->to_where)->get();
        }
        return $product;
    }

    protected function getPrefix()
    {
        if(session()->has('locale')){
            if (session('locale') == 'en') {
                $prefix = 'eng_';
            } else {
                $prefix = session('locale') . '_';
            }
        } else {
            $prefix = 'eng_';
        }

        return $prefix;
    }

    protected function getMappedCategories(Type $type)
    {
        $prefix = $this->getPrefix();

        return Category::select('id',
            $prefix . 'name as name',
            $prefix . 'slug as slug',
            $prefix . 'meta_description as meta_description',
            $prefix . 'description as description',
            'image',
            'anim'
        )->where('type_id', $type->id)->paginate(8);
    }

    protected function getMappedSortedItems()
    {
        $prefix = $this->getPrefix();

        return SortItem::select('id',
            $prefix . 'sort as sort',
            'column',
            'to_where'
        )->get();
    }

    protected function getMappedCategory(Type $type, $categoryId)
    {
        $prefix = $this->getPrefix();

        return Category::select('id',
            $prefix . 'name as name',
            $prefix . 'slug as slug',
            $prefix . 'meta_description as meta_description',
            $prefix . 'description as description',
            'image',
            'anim'
        )->findOrFail($categoryId);
    }

    protected function getMappedTypes(Type $type)
    {
        $prefix = $this->getPrefix();

        return Type::select('id',
            $prefix . 'name as name',
            $prefix . 'slug as slug',
            'anim'
        )->where('id', $type->id)->firstOrFail();
    }

    protected function emptyRequest(Request $request)
    {
        if($request->has('sort') === TRUE){
            $validated = $this->validator($request);
            $sortItem = SortItem::find((int)$validated['sort']);
        } else {
            $cookie = Cookie::get();
            if(empty($cookie['sort'])){
                $sortItem = SortItem::find(1)->firstOrFail();
            } else {
                $sortItem = SortItem::find((int)$cookie['sort']);
            }
        }

        return $sortItem;
    }

    protected function sortValidator()
    {
        $data = [
            'sort' => Session::get('sort'),
        ];

        return Validator::make($data, [
            'sort' => 'exists:sort_items,id',
        ])->validate();
    }

    protected function colorValidator()
    {
        $data = [
            'color' => Session::get('color'),
        ];

        return Validator::make($data, [
            'color' => 'exists:colors,id',
        ])->validate();
    }

    protected function emptySessionSort()
    {
        if(Session::has('sort')){
            $validated = $this->sortValidator();
            $sortItem = SortItem::find((int)$validated['sort']);
        } else {
            $cookie = Cookie::get();
            if(empty($cookie['sort'])){
                $sortItem = SortItem::find(1)->firstOrFail();
            } else {
                $sortItem = SortItem::find((int)$cookie['sort']);
            }
        }
        return $sortItem;
    }

    protected function emptySessionColor()
    {
        if(Session::has('color')){
            $validated = $this->colorValidator();
            $color = Color::find((int)$validated['color']);
        } else {
            $cookie = Cookie::get();
            if(empty($cookie['color'])){
                $color = Color::find(1)->firstOrFail();
            } else {
                $color = Color::find((int)$cookie['color']);
            }
        }
        return $color;
    }

    protected function cookie(SortItem $sortItem, Color $color)
    {
        Cookie::queue('sort', (int)$sortItem->id, '3600');
        Cookie::queue('color', (int)$color->id, '3600');
    }


}

