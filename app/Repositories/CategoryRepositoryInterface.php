<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Type;

interface CategoryRepositoryInterface
{
    public function getTypes(Type $type);

    public function getCategories(Type $type, $categoryId, $plural);

    public function getAllTags();

    public function getAllKeywords();

    public function getAllSortBy();

    public function getAllColors();

    public function getAllProductsSortedBy(Category $category);
}
