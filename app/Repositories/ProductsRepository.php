<?php
namespace App\Repositories;


use App\Models\Color;
use App\Models\Product;
use \Illuminate\Http\Request;

class ProductsRepository implements ProductsRepositoryInterface
{
    public function getProduct(Product $product)
    {
        $prefix = $this->getPrefix();
        return Product::select(
            'id',
            $prefix . 'name as name',
            $prefix . 'slug as slug',
            $prefix . 'meta_description as meta_description',
            $prefix . 'description as description',
            'image', 'price', 'color_id', 'category_id', 'size'
        )->where('id', $product->id)->firstOrFail();
    }

    public function getColorOfProduct(Product $product)
    {
        $prefix = $this->getPrefix();

        return Color::select('id',
            $prefix . 'color as color',
            'image'
        )->where('id', $product->color_id)->firstOrFail();
    }

    public function getRandomProducts()
    {
        $prefix = $this->getPrefix();

        return Product::select(
            'id',
            $prefix . 'name as name',
            $prefix . 'slug as slug',
            $prefix . 'meta_description as meta_description',
            $prefix . 'description as description',
            'image', 'price', 'color_id', 'category_id', 'size'
        )->inRandomOrder()->limit(5)->get();

    }

    public function searchForProducts(Request $request)
    {
        $validated = $this->validateSearch($request);
        $prefix = $this->getPrefix();
        $where = $prefix . 'name';
        return Product::select(
            'id',
            $prefix . 'name as name',
            $prefix . 'slug as slug',
            $prefix . 'meta_description as meta_description',
            $prefix . 'description as description',
            'image', 'price', 'color_id', 'category_id', 'size'
        )->where($where, 'LIKE', '%'. $validated['search'] .'%')->paginate(15);
    }

    protected function validateSearch(Request $request)
    {
        return $request->validate(
            [
                'search' => 'required|min:2|max:50|string',
            ]
        );
    }

    protected function getPrefix()
    {
        if(session()->has('locale')){
            if (session('locale') == 'en') {
                $prefix = 'eng_';
            } else {
                $prefix = session('locale') . '_';
            }
        } else {
            $prefix = 'eng_';
        }

        return $prefix;
    }
}
