<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Http\Request;

interface ProductsRepositoryInterface
{
    public function getProduct(Product $product);

    public function getColorOfProduct(Product $product);

    public function getRandomProducts();

    public function searchForProducts(Request $request);
}
