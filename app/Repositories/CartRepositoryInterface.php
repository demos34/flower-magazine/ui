<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Http\Request;

interface CartRepositoryInterface
{
    public function addCartContent(Request $request, Product $product);

    public function getCartContent();

    public function getTotal();

    public function deleteFromCart($rowId);

    public function updateCart(Request $request);

    public function purchase(Request $request);

    public function sendUsMsg(Request $request);
}
