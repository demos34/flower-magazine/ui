<?php
namespace App\Repositories;


use App\Models\Item;
use App\Models\Message;
use App\Models\Product;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartRepository implements CartRepositoryInterface
{
    public function addCartContent(Request $request, Product $product)
    {
        $prefix = $this->getPrefix();
        $qty = (int)$request->quantity;
        if ($qty < 1) {
            return false;
        }

        $color = implode
            (',', $product->color()->get()->pluck($prefix .'color')->toArray());

        $fullPrice = $qty * $product->price;
        $name = $prefix.'name';
        $cart = Cart::add($product->id, $product->$name, $qty, $product->price, 0,
            [
                'color' => $color,
                'quantity' => $qty,
                'size' => $product->size . 'cm',
                'image' => $product->image,
                'full_price' => $fullPrice,
            ]
        )->associate('Product');

        return $cart;
    }

    public function getCartContent()
    {
        return Cart::content();
    }

    public function getTotal()
    {
        return Cart::subtotal();
    }

    public function deleteFromCart($rowId)
    {
        try {
            Cart::get($rowId);
        } catch (\Exception $exception) {
            return [
                'session' => 'success',
                'message' => 'The product is successfully removed from Cart!'
            ];
        }
        Cart::remove($rowId);
        return [
            'session' => 'success',
            'message' => 'The product is successfully removed from Cart!'
        ];
    }

    public function updateCart(Request $request)
    {
        $cartItems = $this->getCartContent();

        foreach ($cartItems as $item) {
            $newQty = (int)$request['quantity-for-' . $item->id];
            if ($newQty < 1) {
                return [
                    'session' => 'danger',
                    'message' => 'The quantity can not be smaller than 1!',
                ];
            }
            Cart::update($item->rowId, $newQty);
            $fullPrice = $newQty * (int)$item->price;
            Cart::update($item->rowId, ['options' =>
                [
                    'color' => $item->options->color,
                    'size' => $item->options->size,
                    'image' => $item->options->image,
                    'full_price' => $fullPrice,
                ]
            ]);
        }

        return [
            'session' => 'success',
            'message' => 'The products are updated and ready to go!',
        ];
    }

    public function purchase(Request $request)
    {
        $cartItems = $this->getCartContent();
        $alert = '';
        $total = $this->getTotal();
        $cart = $this->addCartToDB($request, $total);
        foreach ($cartItems as $cartItem) {

            $cartQuantity = $cartItem->qty;
            $product = Product::findOrFail($cartItem->id);
            $warehouseQuantity = $product->warehouse->quantity;
            if ($warehouseQuantity < $cartQuantity) {
                $alert .= 'The product ' . $product->eng_name . ': There are fewer pieces than the order!';
            }
            $item = Item::create(
                [
                    'product_id' => $cartItem->id,
                    'quantity' => $cartItem->qty,
                    'price' => $cartItem->price * $cartItem->qty,
                ]
            );
            $cart->products()->attach($cartItem->id);
            $cart->items()->attach($item->id);
            $rowId = $cartItem->rowId;
            Cart::remove($rowId);
        }
        $this->sendMessageToAdministrators($cart->id, $total, $alert);

    }

    public function sendUsMsg(Request $request)
    {
        $validated = $request->validate(
            [
                'email' => 'required|min:5|max:100|email',
                'msg' => 'required|min:5|max:1000|string'
            ]
        );

        $users = User::whereHas('roles', function ($query) {
            return $query->where('role_id', 1);
        })->orWhereHas('roles', function ($query) {
            return $query->where('role_id', 2);
        })->get();
        foreach ($users as $user) {
            Message::create(
                [
                    'from' => $validated['email'],
                    'user_id' => $user->id,
                    'msg' => $validated['msg'],
                    'isRead' => false,
                    'isDeleted' => false,
                ]
            );
        }
    }

    protected function getPrefix()
    {
        if(session()->has('locale')){
            if (session('locale') == 'en') {
                $prefix = 'eng_';
            } else {
                $prefix = session('locale') . '_';
            }
        } else {
            $prefix = 'eng_';
        }

        return $prefix;
    }

    protected function purchaseValidator(Request $request)
    {
        return $request->validate(
            [
                'names' => 'required|min:3|max:500|string',
                'phone' => 'required|min:3|max:500|string',
                'address' => 'required|min:3|max:2000|string',
                'email' => 'required|min:3|max:100|string|email',
                'additional_info' => 'min:3|max:2000|string',
            ]
        );
    }

    protected function addCartToDB(Request $request, $total)
    {
        $validated = $this->purchaseValidator($request);
        return \App\Models\Cart::create(
            [
                'names' => $validated['names'],
                'phone' => $validated['phone'],
                'address' => $validated['address'],
                'email' => $validated['email'],
                'additional_info' => $validated['additional_info'],
                'total' => $total,
                'isAccepted' => false,
                'isShipped' => false,
                'isPaid' => false,
                'isClosed' => false,
                'isDeleted' => false,
            ]
        );
    }

    protected function sendMessageToAdministrators($id, $total, $alert)
    {
        $array = explode('/', Session::previousUrl());
        array_pop($array);
        $newUrl = implode('/', $array) . '/';
        $cart = \App\Models\Cart::findOrFail($id);
        $msg = "From System! \r\n Има нова поръчка от "
            . $cart->names
            . ".\r\n Телефон: "
            . $cart->phone
            . ". \r\n Адрес: "
            . $cart->address
            . ". \r\n Имейл: "
            . $cart->email
            . " .\r\n Продукти: \r\n";

        foreach ($cart->items as $item) {
            $msg .= $item->product->bg_name
                . ' / '
                . $item->product->eng_name
                . ' - ' . $item->quantity
                . ', за сумата от: '
                . $item->price
                . " лв.\r\n Линк към продукта: "
                . $newUrl
                . 'products/'
                . $item->id
                . " \r\n"
                . 'Общо: '
                . $total
                . ' лв.';
        }
        if ($alert == '') {
            $msg .= '\r\n Alerts: No!';
        } else {
            $msg .= '\r\n Alerts:' . $alert;
        }

        $users = User::whereHas('roles', function ($query) {
            return $query->where('role_id', 1);
        })->orWhereHas('roles', function ($query) {
            return $query->where('role_id', 2);
        })->get();
        foreach ($users as $user) {
            Message::create(
                [
                    'from' => 'System',
                    'user_id' => $user->id,
                    'msg' => $msg,
                    'isRead' => false,
                    'isDeleted' => false,
                ]
            );
        }
    }
}
