<?php

use App\Mail\OrdersMail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Auth::routes();

Route::get('/lang/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'bg'])) {
        $locale = 'en';
    }

    session()->put('locale', $locale);
    return redirect()->back();
});


Route::get('/email', function () {
    Mail::to('administrator@leya.bg')->send(new OrdersMail());
    return new OrdersMail();
});


Route::get('/', 'Home\HomeController@index')->name('home');
Route::get('/home', 'Home\HomeController@index')->name('home');
Route::get('/contacts', 'Home\HomeController@about')->name('home');
Route::get('/flowers', 'Home\HomeController@flowers')->name('flowers');
Route::get('/category', 'Home\HomeController@category')->name('cat');
Route::get('/color', 'Home\HomeController@category')->name('cat');
Route::get('/products', 'Home\HomeController@products')->name('home');
Route::post('/message', 'Home\HomeController@message')->name('message');
//Route::get('/cart', 'Home\HomeController@cart')->name('home');

/*
 * Categories
 */


Route::get('/bg/category/index/{type:bg_slug}', 'Category\CategoryController@index')->name('category-index-bg');
Route::get('/en/category/index/{type:eng_slug}', 'Category\CategoryController@index')->name('category-index-bg');;
Route::get('/{locale}/category/index/{type}', function ($locale, $type) {
    if($locale == 'bg'){
        return redirect()->route('category-index-bg', [
            'type' => $type,
        ]);
    } else {
        return redirect()->route('category-index-eng', [
            'type' => $type,
        ]);
    }
});
Route::get('/category/show/{category}/{sortItem}', 'Administrator\CategoryController@viewAllProducts');
Route::get('/bg/category/show/{category:bg_slug}', 'Category\CategoryController@viewAllProducts')->name('category-show-all-bg');
Route::get('/en/category/show/{category:eng_slug}', 'Category\CategoryController@viewAllProducts')->name('category-show-all-en');

Route::get('/{locale}/category/show/{category}', function ($locale, $category) {
    if($locale == 'bg'){
        return redirect()->route('category-show-all-bg', [
            'category' => $category,
        ]);
    } else {
        return redirect()->route('category-show-all-eng', [
            'category' => $category,
        ]);
    }
});


/*
 * Category get colors and sort by
 */
Route::get('{locale}/category/color/{color}/{category}', 'Category\CategoryController@sessionColor');

Route::get('{locale}/category/sort/{sortItem}/{category}', 'Category\CategoryController@sessionSort');



/*
 * Product
 */

Route::get('bg/products/{product:bg_slug}', 'Products\ProductsController@show')->name('product-show-bg');
Route::get('en/products/{product:eng_slug}', 'Products\ProductsController@show')->name('product-show-eng');

Route::get('/{locale}/products/{product}', function ($locale, $product) {
    if($locale == 'bg'){
        return redirect()->route('product-show-bg', [
            'product' => $product,
        ]);
    } else {
        return redirect()->route('product-show-eng', [
            'product' => $product,
        ]);
    }
});

/*
 * search engine
 */

Route::get('/search', 'Products\ProductsController@search')->name('search');


/*
 * cart
 */

Route::get('/cart', 'Cart\CartController@index');

Route::get('/checkout', 'Cart\CartController@checkout');

Route::post('/cart/{product}', 'Cart\CartController@addToCart');

Route::put('/cart', 'Cart\CartController@updateCart');

Route::put('/purchase', 'Cart\CartController@purchase');

Route::get('/cart/delete/{rowId}', 'Cart\CartController@delete');


Route::get('/banner', function () {
    return view('home.banner');
});
Route::get('/slider', function () {
    return view('home.slider');
});


Route::get('/policy/security', 'Policy\PolicyController@security');
Route::get('/policy/summary', 'Policy\PolicyController@summary');
Route::get('/policy/cookies', 'Policy\PolicyController@cookies');
