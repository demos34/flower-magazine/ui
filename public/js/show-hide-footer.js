function showMore(){
    let toShow = document.getElementById('mobile-policy-hidden');
    let showBtn = document.getElementById('show-more');
    let toHide = document.getElementById('contact-mobile');
    toShow.style.display = 'block';
    showBtn.style.display = 'none';
    toHide.style.display = 'none'
}
function hideMore(){
    let toHide = document.getElementById('mobile-policy-hidden');
    let showBtn = document.getElementById('show-more');
    let toShow = document.getElementById('contact-mobile');
    toShow.style.display = 'block';
    showBtn.style.display = 'block';
    toHide.style.display = 'none'
}
