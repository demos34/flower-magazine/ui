const slidersTwo = document.getElementsByClassName('car-two');
const imagesTwo = document.getElementsByClassName('car-product-image-two')
let buttonStartTwo = document.getElementsByClassName('btn-car-prev-two');
buttonStartTwo[0].classList.add('isnt-display-two');
viewCount = 0;
if (imagesTwo[0].offsetWidth < 250){
    viewCountTwo = 0;
    const countImageTwo = 1;
} else {
    viewCountTwo = 2;
    const countImageTwo = 3;
}

const checkCountsTwo = viewCountTwo;

for (let itemTwo of slidersTwo){
    itemTwo.classList.remove('is-display-two');
    itemTwo.classList.add('isnt-display-two');
}

for (i = 0; i <= viewCountTwo; i++){
    slidersTwo[i].classList.remove('isnt-display-two');
    slidersTwo[i].classList.add('is-display-two');
}

imageOneTwo = document.getElementById('1-two');
setTwoHighToElement(imageOneTwo);


function nTwo(){
    let visibleTwo = document.getElementsByClassName('is-display-two');
    let lastVisibleTwo = visibleTwo[visibleTwo.length - 1];
    lastVisibleTwo.nextElementSibling.classList.remove('isnt-display-two');
    lastVisibleTwo.nextElementSibling.classList.add('is-display-two');
    visibleTwo[0].classList.add('isnt-display-two');
    visibleTwo[0].classList.remove('is-display-two');
    checkTwoLastElement(visibleTwo);
}

function pTwo(){
    let visibleTwo = document.getElementsByClassName('is-display-two');
    let lastVisibleTwo = visibleTwo[visibleTwo.length - 1];
    visibleTwo[0].previousElementSibling.classList.remove('isnt-display-two');
    visibleTwo[0].previousElementSibling.classList.add('is-display-two');
    lastVisibleTwo.classList.add('isnt-display-two');
    lastVisibleTwo.classList.remove('is-display-two');
    checkTwoFirstElement(visibleTwo);
}

function checkTwoLastElement(visibleTwo){
    let lastVisibleTwo = visibleTwo[visibleTwo.length - 1]
    if(lastVisibleTwo.nextElementSibling == null){
        let bTwo = document.getElementsByClassName('btn-car-next-two');
        bTwo[0].classList.add('isnt-display-two');
    }
    let bPrevTwo = document.getElementsByClassName('btn-car-prev-two');
    bPrevTwo[0].classList.remove('isnt-display-two');
    highTwoLighted(visibleTwo);
}

function checkTwoFirstElement(visibleTwo){
    if(visibleTwo[0].previousElementSibling == null){
        let bPrevTwo = document.getElementsByClassName('btn-car-prev-two');
        bPrevTwo[0].classList.add('isnt-display-two');
    }
    let bTwo = document.getElementsByClassName('btn-car-next-two');
    bTwo[0].classList.remove('isnt-display-two');
    highTwoLighted(visibleTwo);
}

function highTwoLighted(visibleTwo){
    if(checkCountsTwo > 1){
        let elementTwo = getTwoElementToHigh(visibleTwo);
        let highElementTwo = getTwoHighElement();
        setTwoHighToElement(elementTwo[1]);
        removeTwoHighFromElement(highElementTwo);
    }
}

function removeTwoHighFromElement(elementTwo){
    elementTwo.style.width = "7em";
    elementTwo.style.height = "7em";
    elementTwo.style.marginTop = "1em";
    elementTwo.style.borderRadius = "0.25rem";
    elementTwo.removeAttribute('id');
    return true;
}

function setTwoHighToElement(elementTwo){
    elementTwo.style.width = "10em";
    elementTwo.style.height = "10em";
    elementTwo.style.marginTop = "-1em";
    elementTwo.style.borderRadius = "0.25rem";
    elementTwo.id = "high-two";
    return true;
}

function getTwoHighElement(){
    return document.getElementById('high-two');
}

function getTwoElementToHigh(visibleTwo){
    let arrayChildTwo = visibleTwo[1].childNodes;
    return arrayChildTwo[1].childNodes;
}


