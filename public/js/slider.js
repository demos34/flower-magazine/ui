const sliders = document.getElementsByClassName('sliders');
const images = document.getElementsByClassName('slider-product-image')
let buttonStart = document.getElementsByClassName('btn-prev');
buttonStart[0].classList.add('isnt-visible');
viewCount = 0;
if (images[0].offsetWidth < 250){
    viewCount = 0;
    const countImage = 1;
} else {
    viewCount = 2;
    const countImage = 3;
}

const checkCounts = viewCount;

for (let item of sliders){
    item.classList.remove('is-visible');
    item.classList.add('isnt-visible');
}

for (i = 0; i <= viewCount; i++){
    sliders[i].classList.remove('isnt-visible');
    sliders[i].classList.add('is-visible');
}

imageOne = document.getElementById(1);
setHighToElement(imageOne);


function next(){
    let visible = document.getElementsByClassName('is-visible');
    let lastVisible = visible[visible.length - 1];
    lastVisible.nextElementSibling.classList.remove('isnt-visible');
    lastVisible.nextElementSibling.classList.add('is-visible');
    visible[0].classList.add('isnt-visible');
    visible[0].classList.remove('is-visible');
    checkLastElement(visible);
}

function prev(){
    let visible = document.getElementsByClassName('is-visible');
    let lastVisible = visible[visible.length - 1];
    visible[0].previousElementSibling.classList.remove('isnt-visible');
    visible[0].previousElementSibling.classList.add('is-visible');
    lastVisible.classList.add('isnt-visible');
    lastVisible.classList.remove('is-visible');
    checkFirstElement(visible);
}

function checkLastElement(visible){
    let lastVisible = visible[visible.length - 1]
    if(lastVisible.nextElementSibling == null){
        let button = document.getElementsByClassName('btn-next');
        button[0].classList.add('isnt-visible');
    }
    let buttonPrev = document.getElementsByClassName('btn-prev');
    buttonPrev[0].classList.remove('isnt-visible');
    highlighted(visible);
}

function checkFirstElement(visible){
    if(visible[0].previousElementSibling == null){
        let buttonPrev = document.getElementsByClassName('btn-prev');
        buttonPrev[0].classList.add('isnt-visible');
    }
    let button = document.getElementsByClassName('btn-next');
    button[0].classList.remove('isnt-visible');
    highlighted(visible);
}

function highlighted(visible){
    if(checkCounts > 1){
        let element = getElementToHigh(visible);
        let highElement = getHighElement();
        setHighToElement(element[1]);
        removeHighFromElement(highElement);
    }
}

function removeHighFromElement(element){
    element.style.width = "7em";
    element.style.height = "7em";
    element.style.marginTop = "1em";
    element.style.borderRadius = "0.25rem";
    element.removeAttribute('id');
    return true;
}

function setHighToElement(element){
    element.style.width = "10em";
    element.style.height = "10em";
    element.style.marginTop = "-1em";
    element.style.borderRadius = "0.25rem";
    element.id = "high";
    return true;
}

function getHighElement(){
    return document.getElementById('high');
}

function getElementToHigh(visible){
    let arrayChild = visible[1].childNodes;
    return arrayChild[1].childNodes;
}


